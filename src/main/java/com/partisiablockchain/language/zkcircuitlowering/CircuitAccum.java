package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuitAppendable;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import com.partisiablockchain.language.zkmetacircuit.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Wrapper for constructing circuits, providing a better-typed interface with support for {@link
 * Operand}, and {@link Type}.
 */
final class CircuitAccum {

  private final ZkCircuitAppendable innerCircuit;
  private final ArrayList<ZkAddress> outputGates = new ArrayList<>();

  /**
   * Constructor for {@link CircuitAccum}.
   *
   * @param innerCircuit Circuit to append new operations to.
   */
  public CircuitAccum(final ZkCircuitAppendable innerCircuit) {
    this.innerCircuit = requireNonNull(innerCircuit);
  }

  public Operand.SecretVariable addBinop(
      final Type.SecretBinaryInteger resultType,
      final ZkOperation.BinaryOp opcode,
      final Operand.SecretVariable v1,
      final Operand.SecretVariable v2) {
    final ZkType.Sbi zkType = toZkType(resultType);
    return new Operand.SecretVariable(
        innerCircuit.add(
            zkType, ZkOperation.Binary.newNormalized(opcode, v1.gateAddress(), v2.gateAddress())),
        resultType);
  }

  public Operand.SecretVariable addUnop(
      final Type.SecretBinaryInteger resultType,
      final ZkOperation.UnaryOp opcode,
      final Operand.SecretVariable v1) {
    final ZkType.Sbi zkType = toZkType(resultType);
    return new Operand.SecretVariable(
        innerCircuit.add(zkType, new ZkOperation.Unary(opcode, v1.gateAddress())), resultType);
  }

  public Operand.SecretVariable addSignExtend(
      final Type.SecretBinaryInteger resultType, final Operand.SecretVariable v1) {
    // Special case for variables already with the correct type.
    if (v1.type().equals(resultType)) {
      return v1;
    }

    // Normal case
    final ZkType.Sbi zkType = toZkType(resultType);
    return new Operand.SecretVariable(
        innerCircuit.add(zkType, new ZkOperation.SignExtend(zkType, v1.gateAddress())), resultType);
  }

  public Operand.SecretVariable addLoad(
      final Type.SecretBinaryInteger resultType, final int loadVariableId) {
    final ZkType.Sbi zkType = toZkType(resultType);
    return new Operand.SecretVariable(
        innerCircuit.add(
            zkType, new ZkOperation.Load(zkType, new ZkOperation.ZkVariableId(loadVariableId))),
        resultType);
  }

  public Operand.SecretVariable addSelect(
      final Type.SecretBinaryInteger resultType,
      final Operand.SecretVariable cnd,
      final Operand.SecretVariable thn,
      final Operand.SecretVariable els) {

    final var thnBitmask = addSignExtend(resultType, cnd);
    final var elsBitmask = addUnop(resultType, ZkOperation.UnaryOp.BITWISE_NOT, thnBitmask);

    final var thnBitmasked =
        addBinop(resultType, ZkOperation.BinaryOp.BITWISE_AND, thn, thnBitmask);
    final var elsBitmasked =
        addBinop(resultType, ZkOperation.BinaryOp.BITWISE_AND, els, elsBitmask);

    return addBinop(resultType, ZkOperation.BinaryOp.BITWISE_XOR, thnBitmasked, elsBitmasked);
  }

  public Operand.SecretVariable addExtract(
      final Type.SecretBinaryInteger resultType,
      final Operand.SecretVariable v1,
      final int bitOffset) {
    final ZkType.Sbi zkType = toZkType(resultType);
    return new Operand.SecretVariable(
        innerCircuit.add(
            zkType, new ZkOperation.Extract(v1.gateAddress(), resultType.width(), bitOffset)),
        resultType);
  }

  public Operand.SecretVariable addConstant(
      final Type.SecretBinaryInteger resultType, final BigInteger constantValue) {
    final ZkType.Sbi zkType = toZkType(resultType);
    final List<Boolean> constantBits =
        IntStream.range(0, resultType.width()).mapToObj(constantValue::testBit).toList();

    return new Operand.SecretVariable(
        innerCircuit.add(zkType, new ZkOperation.Constant(zkType, constantBits)), resultType);
  }

  private static ZkType.Sbi toZkType(Type.SecretBinaryInteger loweringType) {
    return new ZkType.Sbi(loweringType.width());
  }

  public void addOutputs(final List<Operand.SecretVariable> value) {
    outputGates.addAll(value.stream().map(Operand.SecretVariable::gateAddress).toList());
  }

  public void setOutputs() {
    innerCircuit.setRoots(outputGates);
  }
}
