package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;

/**
 * Injected dependency to expose public data to the {@link MetaCircuitLowering} class.
 *
 * <p>None of the calls are allowed to throw any exceptions.
 */
public interface LoweringPublicExecutionEnvironment {

  /**
   * Get metadata for a secret variable. This one hides the low-level structure for metadata by
   * encoding to an integer.
   *
   * @param variableId Id of the secret variable to fetch metadata for.
   * @return Metadata as unsigned {@link BigInteger}. Null if the variable does not exist.
   */
  BigInteger getSecretVariableMetadata(final int variableId);

  /**
   * Determines the id of the subsequent secret variable after the given {@code variableId}.
   *
   * <p>Behaviour is specified as:
   *
   * <ul>
   *   <li>If {@code variableId == 0}: Return the id of the first variable.
   *   <li>If {@code variableId != 0}: Return id of the subsequent variable.
   *   <li>If no subsequent variable: Return 0.
   * </ul>
   *
   * @param variableId Current id.
   * @return Subsequent id.
   */
  int nextVariableId(int variableId);

  /**
   * Exposes number of variables available to the circuit for computation.
   *
   * @return Number of variables.
   */
  int numberOfSecretVariables();
}
