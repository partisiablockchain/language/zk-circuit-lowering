package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitImpl;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkmetacircuit.Block;
import com.partisiablockchain.language.zkmetacircuit.BlockId;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.Instruction;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.Operation;
import com.partisiablockchain.language.zkmetacircuit.Terminator;
import com.partisiablockchain.language.zkmetacircuit.Type;
import com.partisiablockchain.language.zkmetacircuit.VariableId;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Executes {@link MetaCircuit}s to {@link ZkCircuit}s.
 *
 * <p>WARNING: This class performs little if any validation, and the resulting {@link ZkCircuit}
 * might be malformed, if the given Meta-Circuit have not previously been validated through {@link
 * com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidator}.
 *
 * <p>The MetaCircuitLowering process can be thought of as a hugely complicated macro language. It
 * takes a Meta-Circuit with support for various types of computations, branches and loops, and need
 * to execute it to a linear circuit, evaluating and inlining all public values, smoothing out
 * branches to a straight line circuit. The resulting program will only be operating on secret
 * variables, and any public data required for the execution have been inlined during the lowering
 * process.
 *
 * <p>The translation process is guarenteed to terminate, even for non-validated meta-circuits,
 * though it may result in an exception.
 */
public final class MetaCircuitLowering {

  private MetaCircuitLowering() {}

  /** Which id the variables start at. */
  private static final int INITIAL_INPUT_ID = 1;

  /** Id of the initial {@link Block} to execute in each function. */
  private static final BlockId INITIAL_BLOCK_ID = new BlockId(0);

  /**
   * Lowering result. Contains both the produced {@link ZkCircuit} and a list of public output
   * values.
   */
  public record Result(ZkCircuit zkCircuit, List<BigInteger> outputValues) {}

  /**
   * Executes Meta-Circuit to ZkCircuit.
   *
   * @param metaCircuit Meta-Circuit to execute.
   * @param functionId Id of function to start execution for.
   * @param publicEnvironment Public environment that the execution/translation should occur in.
   * @param inputValues Public inputs values to compute on. Should be unsigned {@link BigInteger}s.
   * @param maximumNumberBranches The maximum number of branch jumps that will be executed before
   *     timing out, and aborting the lowering process, in order to avoid being stuck in infinite
   *     loops.
   * @return Produced ZkCircuit
   */
  public static Result executeMetaCircuit(
      final MetaCircuit metaCircuit,
      final FunctionId functionId,
      final LoweringPublicExecutionEnvironment publicEnvironment,
      final List<BigInteger> inputValues,
      final long maximumNumberBranches) {
    // Basic validation
    if (metaCircuit.functions().isEmpty()) {
      throw new RuntimeException("Meta-Circuit must contain at least one function");
    }

    // Block traversal utility
    final Map<FunctionId, Map<BlockId, Block>> blockMap = determineBlockMap(metaCircuit);

    // Initialize circuit and environment
    final ZkCircuitImpl resultCircuit = new ZkCircuitImpl();
    final CircuitAccum circuitAccum = new CircuitAccum(resultCircuit);

    // Execute Meta-Circuit
    final List<Operand> outputOperands =
        executeMetaCircuitInternal(
            functionId,
            blockMap,
            publicEnvironment,
            inputValues,
            circuitAccum,
            maximumNumberBranches);

    // Finalize Zk-Circuit

    final List<Operand.SecretVariable> outputGatesByReturn =
        outputOperands.stream()
            .filter(x -> x instanceof Operand.SecretVariable)
            .map(x -> (Operand.SecretVariable) x)
            .toList();

    final List<BigInteger> publicOutputValues =
        outputOperands.stream()
            .filter(x -> x instanceof Operand.PublicConstant)
            .map(x -> (Operand.PublicConstant) x)
            .map(Operand.PublicConstant::value)
            .toList();

    circuitAccum.addOutputs(outputGatesByReturn);
    circuitAccum.setOutputs();

    return new Result(resultCircuit, publicOutputValues);
  }

  /**
   * Produces a map from function and block ids to the map with that function and block id.
   *
   * @param metaCircuit Meta-circuit to determine from.
   * @return Map from function and block ids to the block. Never null.
   */
  private static Map<FunctionId, Map<BlockId, Block>> determineBlockMap(
      final MetaCircuit metaCircuit) {
    final Map<FunctionId, Map<BlockId, Block>> blockMap = new HashMap<>();
    for (final MetaCircuit.Function function : metaCircuit.functions()) {
      blockMap.put(
          function.functionId(),
          function.blocks().stream()
              .collect(Collectors.toUnmodifiableMap(Block::blockId, Function.identity())));
    }
    return Map.copyOf(blockMap);
  }

  /**
   * Represents a single entry on the return stack.
   *
   * @param functionId Id of function to return to.
   * @param blockId Id of block to return to.
   * @param prependArguments Arguments to prepend to the target block's arguments. Return values for
   *     the returning function will be placed after these.
   */
  private record ReturnStackEntry(
      FunctionId functionId, BlockId blockId, List<Operand> prependArguments) {

    /** Constructor for {@link ReturnStackEntry}. */
    public ReturnStackEntry {
      requireNonNull(functionId);
      requireNonNull(blockId);
      requireNonNull(prependArguments);
    }
  }

  private static List<Operand> executeMetaCircuitInternal(
      FunctionId currentFunctionId,
      final Map<FunctionId, Map<BlockId, Block>> blockMap,
      final LoweringPublicExecutionEnvironment publicEnvironment,
      final List<BigInteger> inputValues,
      final CircuitAccum circuitAccum,
      long branchGasLeft) {

    final ArrayDeque<ReturnStackEntry> returnStack = new ArrayDeque<>();

    BlockId currentBlockId = INITIAL_BLOCK_ID;
    Environment environment =
        environmentForComputationInputs(
            getBlock(blockMap, currentFunctionId, currentBlockId).inputs(),
            inputValues,
            circuitAccum);

    for (; true; branchGasLeft--) {
      final Block currentBlock = getBlock(blockMap, currentFunctionId, currentBlockId);
      // Executes instructions
      for (final Instruction insn : currentBlock.instructions()) {
        final Operand resultingOperand =
            executeOperation(
                insn.resultType(), insn.operation(), circuitAccum, environment, publicEnvironment);
        environment.add(insn.resultId(), insn.resultType(), resultingOperand);
      }

      // Executes terminator

      if (currentBlock.terminator() instanceof Terminator.Call terminator) {
        // Call stuff
        final List<Operand> argumentForReturnToBlock =
            resolveVariables(terminator.branchToAfterReturn().branchInputVariables(), environment);

        returnStack.addLast(
            new ReturnStackEntry(
                currentFunctionId,
                terminator.branchToAfterReturn().targetBlock(),
                argumentForReturnToBlock));

        currentFunctionId = terminator.functionId();
        currentBlockId = INITIAL_BLOCK_ID;

        environment =
            environmentForNextBlock(
                getBlock(blockMap, currentFunctionId, currentBlockId).inputs(),
                terminator.functionArguments(),
                environment);

      } else {

        final Terminator.BranchInfo terminatorBranch =
            evaluateTerminatorForBranchInfo(currentBlock.terminator(), environment);

        currentBlockId = terminatorBranch.targetBlock();
        List<Operand> targetBlockArguments =
            resolveVariables(terminatorBranch.branchInputVariables(), environment);

        // Perform unwinding by popping of the stack for all returns (branches to RETURN.)
        while (BlockId.RETURN.equals(currentBlockId)) {
          if (returnStack.isEmpty()) {
            // Once the stack hits empty is can return computation results.
            return targetBlockArguments;
          } else {
            final ReturnStackEntry returnStackFrame = returnStack.removeLast();
            currentFunctionId = returnStackFrame.functionId();
            currentBlockId = returnStackFrame.blockId();
            final List<Operand> inputs = new ArrayList<>(returnStackFrame.prependArguments);
            inputs.addAll(targetBlockArguments);
            targetBlockArguments = inputs;
          }
        }

        // Once all returns have been resolved, we can branch to the target block.
        // Update block and environment
        environment =
            environmentForNextBlock(
                getBlock(blockMap, currentFunctionId, currentBlockId).inputs(),
                targetBlockArguments);
      }
      if (branchGasLeft == 0) {
        throw new RuntimeException(
            "Too many branches have been performed! Might be in an infinite loop? Aborting!");
      }
    }
  }

  static Block getBlock(
      Map<FunctionId, Map<BlockId, Block>> blockMap, FunctionId functionId, BlockId blockId) {
    final Map<BlockId, Block> functionBlocks = blockMap.get(functionId);
    if (functionBlocks == null) {
      throw new RuntimeException("No function with id %s".formatted(functionId.functionId()));
    }
    final Block block = functionBlocks.get(blockId);
    if (block == null) {
      throw new RuntimeException(
          "No block with id %s in function %s"
              .formatted(blockId.blockId(), functionId.functionId()));
    }
    return block;
  }

  private static Environment environmentForComputationInputs(
      List<Block.Input> blockInputs, List<BigInteger> inputValues, CircuitAccum circuitAccum) {

    // Determine whether enough inputs have been given
    final List<Type.PublicInteger> publicInputTypes = publicInputTypes(blockInputs);

    if (publicInputTypes.size() != inputValues.size()) {
      throw new RuntimeException(
          "Public input count mismatch. Expected %s, gotten %s"
              .formatted(publicInputTypes, inputValues));
    }

    // Create initial environment
    int publicInputIdx = 0;
    int secretInputId = INITIAL_INPUT_ID;

    final Environment environment = Environment.empty();
    for (final Block.Input input : blockInputs) {
      final Operand resultOperand;

      // Public integer
      if (input.variableType() instanceof Type.PublicInteger variableType) {
        final BigInteger value = inputValues.get(publicInputIdx++);
        resultOperand = unsignedPublicConstant(value, variableType);

        // Secret Binary integer
      } else {
        final Type.SecretBinaryInteger variableType =
            (Type.SecretBinaryInteger) input.variableType();
        resultOperand = circuitAccum.addLoad(variableType, secretInputId++);
      }

      environment.add(input.assignedVariable(), input.variableType(), resultOperand);
    }

    return environment;
  }

  static List<Type.PublicInteger> publicInputTypes(final List<Block.Input> blockInputs) {
    return blockInputs.stream()
        .map(x -> x.variableType() instanceof Type.PublicInteger y ? y : null)
        .filter(x -> x != null)
        .toList();
  }

  static List<Type.PublicInteger> publicInputTypes(final Block block) {
    return publicInputTypes(block.inputs());
  }

  /**
   * Creates a new environment for the next block, by mapping variables to input definitions.
   *
   * @param expectedInputs Input variable definitions, which will initially populate the new
   *     environment.
   * @param argumentVariables Variables given as arguments to the block.
   * @param argumentEnvironment Environment for the block where the arguments are contained.
   * @return Newly created environment, initially only populated by the input variables.
   */
  private static Environment environmentForNextBlock(
      final List<Block.Input> expectedInputs,
      final List<VariableId> argumentVariables,
      final Environment argumentEnvironment) {
    return environmentForNextBlock(
        expectedInputs, resolveVariables(argumentVariables, argumentEnvironment));
  }

  private static Environment environmentForNextBlock(
      final List<Block.Input> expectedInputs, final List<Operand> argumentValues) {
    final Environment newEnv = Environment.empty();
    for (int inputIdx = 0; inputIdx < expectedInputs.size(); inputIdx++) {
      final Block.Input input = expectedInputs.get(inputIdx);
      final Operand argumentValue = argumentValues.get(inputIdx);
      newEnv.add(input.assignedVariable(), input.variableType(), argumentValue);
    }

    return newEnv;
  }

  private static List<Operand> resolveVariables(
      final List<VariableId> argumentVariables, final Environment argumentEnvironment) {
    return argumentVariables.stream().map(argumentEnvironment::get).toList();
  }

  /**
   * Ensures that the given {@link BigInteger} fits in a type of the given bitwidth.
   *
   * @param signedValue Value to convert.
   * @param typeSize Size of the type to fit into.
   * @return Value. {@code 0 <= value < 2 pow typeSize}
   */
  static BigInteger unsignedBigInteger(int signedValue, int typeSize) {
    return PublicValueExecution.fitInBits(typeSize, BigInteger.valueOf(signedValue));
  }

  /**
   * Converts an signed {@link BigInteger} to an unsigned {@link BigInteger}. Should be used in most
   * cases where public values are constructed from outside inputs.
   *
   * @param value Value to make unsigned. Already unsigned values are not changed.
   * @param type Type to fit into.
   */
  static Operand.PublicConstant unsignedPublicConstant(BigInteger value, Type.PublicInteger type) {
    return PublicValueExecution.fitInType(type, value);
  }

  /**
   * Executes operation to a relevant gate (or sequence of gates.) Returns the address of the newly
   * created gate.
   *
   * <p>Operations over public variables will be delegated to {@link PublicValueExecution}, and the
   * result will be returned as an {@link Operand.PublicConstant}. The instruction will not (and
   * cannot) be emitted.
   *
   * @param uncheckedOperation Operation to execute.
   * @param circuitAccum Circuit to add new gates into.
   * @param environment Environment tracking variables and their gates.
   * @return Operand, tracking either a gate address, or a constant value.
   */
  private static Operand executeOperation(
      final Type uncheckedResultType,
      final Operation uncheckedOperation,
      final CircuitAccum circuitAccum,
      final Environment environment,
      final LoweringPublicExecutionEnvironment publicEnvironment) {

    // Constant operation
    if (uncheckedOperation instanceof Operation.Constant operation) {
      final BigInteger constant =
          unsignedBigInteger(operation.const1(), uncheckedResultType.width());

      if (uncheckedResultType instanceof Type.PublicInteger resultType) {
        return unsignedPublicConstant(constant, resultType);
      } else {
        final Type.SecretBinaryInteger resultType = (Type.SecretBinaryInteger) uncheckedResultType;
        return circuitAccum.addConstant(resultType, constant);
      }

      // Select operation
    } else if (uncheckedOperation instanceof Operation.Select operation) {
      final Operand uncheckedCnd = environment.get(operation.varCondition());
      final Operand uncheckedThen = environment.get(operation.varIfTrue());
      final Operand uncheckedElse = environment.get(operation.varIfFalse());

      if (uncheckedCnd instanceof Operand.PublicConstant operandCnd) {
        if (!operandCnd.type().equals(Type.PublicBool)) {
          throw new RuntimeException("Select on public non-i1 variable!");
        }
        return PublicValueExecution.evaluateSelect(
            operandCnd, uncheckedThen.expectConstant(), uncheckedElse.expectConstant());
      } else {
        final Operand.SecretVariable operandCnd = (Operand.SecretVariable) uncheckedCnd;
        return circuitAccum.addSelect(
            uncheckedThen.expectVariable().type(),
            operandCnd.expectVariable(),
            uncheckedThen.expectVariable(),
            uncheckedElse.expectVariable());
      }

      // Extract operation
    } else if (uncheckedOperation instanceof Operation.Extract operation) {
      final Operand operExtractFrom = environment.get(operation.varIdx1());
      final int offsetFromLow = operation.offsetFromLow();
      return executeExtractOperation(
          uncheckedResultType, circuitAccum, operExtractFrom, offsetFromLow);

      // Extract Dynamic operation
    } else if (uncheckedOperation instanceof Operation.ExtractDynamically operation) {
      final Operand operExtractFrom = environment.get(operation.varIdxExtractFrom());
      final Operand operOffsetFromLow = environment.get(operation.varIdxOffsetFromLow());
      final int offsetFromLow = operOffsetFromLow.expectConstant().value().intValue();
      return executeExtractOperation(
          uncheckedResultType, circuitAccum, operExtractFrom, offsetFromLow);

      // Insert Dynamic operation
    } else if (uncheckedOperation instanceof Operation.InsertDynamically operation) {
      final Operand operInsertInto = environment.get(operation.varIdxInsertInto());
      final Operand operInserted = environment.get(operation.varIdxInserted());
      final Operand operOffsetFromLow = environment.get(operation.varIdxOffsetFromLow());
      return executeDynamicInsert(circuitAccum, operInsertInto, operInserted, operOffsetFromLow);

      // Binary operation
    } else if (uncheckedOperation instanceof Operation.Binary operation) {
      final Operand v1 = environment.get(operation.varIdx1());
      final Operand v2 = environment.get(operation.varIdx2());
      return executeBinaryOperation(
          uncheckedResultType, circuitAccum, operation.operation(), v1, v2);

      // Nullary operation
    } else if (uncheckedOperation instanceof Operation.Nullary operation) {
      return executeNullary(
          (Type.PublicInteger) uncheckedResultType, /*operation.operation(),*/ publicEnvironment);

      // Cast operation
    } else if (uncheckedOperation instanceof Operation.Cast operation) {
      final Operand v1 = environment.get(operation.varIdx1());

      // Casting from public to secret
      if (operation.type() instanceof Type.SecretBinaryInteger resultType
          && v1 instanceof Operand.PublicConstant operand) {
        return circuitAccum.addConstant(resultType, operand.value());

        // Cast to same type is identity operation
      } else if (operation.type().equals(v1.type())) {
        return v1;

        // Unknown or disallowed type cast
      } else {
        throw new RuntimeException(
            "Casting between these types not allowed:\n    From : %s\n    To   : %s\n    Oper : %s"
                .formatted(v1.type(), operation.type(), operation.varIdx1()));
      }

      // Unary operation
    } else {
      final Operation.Unary operation = (Operation.Unary) uncheckedOperation;
      final Operand v1 = environment.get(operation.varIdx1());

      // Loading metadata
      if (operation.operation().equals(Operation.UnaryOp.LOAD_METADATA)) {
        final int variableId = v1.expectConstant().value().intValue();
        final BigInteger metadataValue = publicEnvironment.getSecretVariableMetadata(variableId);
        if (metadataValue == null) {
          throw new RuntimeException("Could not load metadata for secret variable " + variableId);
        }
        return unsignedPublicConstant(metadataValue, (Type.PublicInteger) uncheckedResultType);

        // Next variable id
      } else if (operation.operation().equals(Operation.UnaryOp.NEXT_VARIABLE_ID)) {
        final int thisVariableId = v1.expectConstant().value().intValue();
        final int nextVariableId = publicEnvironment.nextVariableId(thisVariableId);
        return unsignedPublicConstant(
            BigInteger.valueOf(nextVariableId), (Type.PublicInteger) uncheckedResultType);

        // Loading variable
      } else if (operation.operation().equals(Operation.UnaryOp.LOAD_VARIABLE)) {
        final int variableId = v1.expectConstant().value().intValue();
        return circuitAccum.addLoad((Type.SecretBinaryInteger) uncheckedResultType, variableId);

        // Output variable
      } else if (operation.operation().equals(Operation.UnaryOp.OUTPUT)) {
        circuitAccum.addOutputs(List.of(v1.expectVariable()));
        return Operand.PublicConstant.UNIT;

        // Public
      } else if (uncheckedResultType instanceof Type.PublicInteger resultType) {
        return PublicValueExecution.evaluateUnop(
            resultType, operation.operation(), v1.expectConstant());

        // Secret
      } else {
        final Type.SecretBinaryInteger resultType = (Type.SecretBinaryInteger) uncheckedResultType;
        return executeSecretUnary(
            circuitAccum, resultType, operation.operation(), v1.expectVariable());
      }
    }
  }

  private static Operand executeDynamicInsert(
      final CircuitAccum circuitAccum,
      final Operand operInsertInto,
      final Operand operInserted,
      final Operand operOffsetFromLow) {

    // Setup
    final Type typeInsertedInto = operInsertInto.type();
    final int offsetFromLow = operOffsetFromLow.expectConstant().value().intValue();

    final Type typeLowBits = setWidth(typeInsertedInto, offsetFromLow);
    final Type typeWithLowBits =
        setWidth(typeInsertedInto, operInserted.type().width() + typeLowBits.width());
    final Type typeHighBits =
        setWidth(typeInsertedInto, operInsertInto.type().width() - typeWithLowBits.width());

    // Extract
    final Operand lowBits = executeExtractOperation(typeLowBits, circuitAccum, operInsertInto, 0);
    final Operand highBits =
        executeExtractOperation(
            typeHighBits,
            circuitAccum,
            operInsertInto,
            offsetFromLow + operInserted.type().width());

    // Concat
    final Operand withLowBits =
        executeBinaryOperation(
            typeWithLowBits, circuitAccum, Operation.BinaryOp.BIT_CONCAT, operInserted, lowBits);
    final Operand result =
        executeBinaryOperation(
            typeInsertedInto, circuitAccum, Operation.BinaryOp.BIT_CONCAT, highBits, withLowBits);
    return result;
  }

  private static Operand executeBinaryOperation(
      final Type uncheckedResultType,
      final CircuitAccum circuitAccum,
      final Operation.BinaryOp binop,
      final Operand v1,
      final Operand v2) {

    // General public binops
    if (uncheckedResultType instanceof Type.PublicInteger resultType) {
      return PublicValueExecution.evaluateBinop(
          resultType, binop, v1.expectConstant(), v2.expectConstant());

      // Mixed bitshifts
    } else if (BITSHIFT_OPERATIONS.contains(binop)
        && v1 instanceof Operand.SecretVariable v1Secret
        && v2 instanceof Operand.PublicConstant v2Public) {
      final Type.SecretBinaryInteger resultType = (Type.SecretBinaryInteger) uncheckedResultType;
      final int shiftAmount = v2Public.value().intValue();
      return executeMixedBitshift(circuitAccum, resultType, binop, v1Secret, shiftAmount);

      // General secret binops
    } else {
      final Type.SecretBinaryInteger resultType = (Type.SecretBinaryInteger) uncheckedResultType;
      return executeSecretBinary(
          circuitAccum, resultType, binop, v1.expectVariable(), v2.expectVariable());
    }
  }

  private static Operand executeExtractOperation(
      final Type uncheckedResultType,
      final CircuitAccum circuitAccum,
      final Operand uncheckedExtractFrom,
      final int offsetFromLow) {

    // Public
    if (uncheckedExtractFrom instanceof Operand.PublicConstant operandVar1) {
      return PublicValueExecution.evaluateExtract(
          operandVar1, (Type.PublicInteger) uncheckedResultType, offsetFromLow);

      // Secret
    } else {
      final Operand.SecretVariable operandVar1 = (Operand.SecretVariable) uncheckedExtractFrom;
      return circuitAccum.addExtract(
          new Type.SecretBinaryInteger(uncheckedResultType.width()), operandVar1, offsetFromLow);
    }
  }

  /**
   * Creates a new type with the same kind as the given type, and the given width.
   *
   * @param uncheckedType Type with the desired kind.
   * @param width Desired width.
   */
  static Type setWidth(final Type uncheckedType, final int width) {
    return uncheckedType instanceof Type.SecretBinaryInteger t
        ? new Type.SecretBinaryInteger(width)
        : new Type.PublicInteger(width);
  }

  private static final Set<Operation.BinaryOp> BITSHIFT_OPERATIONS =
      Set.of(Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL, Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL);

  private static Operand.PublicConstant executeNullary(
      final Type.PublicInteger resultType,
      // final Operation.NullaryOp operation,
      final LoweringPublicExecutionEnvironment publicEnvironment) {

    // Number secret variables
    // (Jacoco prevents including a branch here)
    // if (operation.equals(Operation.NullaryOp.NUM_VARIABLES)) {
    return new Operand.PublicConstant(
        BigInteger.valueOf(publicEnvironment.numberOfSecretVariables()), resultType);
    // }
  }

  private static Operand executeMixedBitshift(
      final CircuitAccum circuitAccum,
      final Type.SecretBinaryInteger resultType,
      final Operation.BinaryOp operation,
      final Operand.SecretVariable v1,
      final int shiftAmount) {

    // Avoid emitting 0-shifts.
    if (shiftAmount == 0) {
      return v1;
    }

    // Large shifts emit zero.
    if (shiftAmount >= resultType.width()) {
      return circuitAccum.addConstant(resultType, BigInteger.ZERO);
    }

    // Shifts within word use extract&concat pattern.

    final Operand.SecretVariable shiftedInZeroBits =
        circuitAccum.addConstant(new Type.SecretBinaryInteger(shiftAmount), BigInteger.ZERO);

    final Type.SecretBinaryInteger typeOfWhatToRetain =
        new Type.SecretBinaryInteger(resultType.width() - shiftAmount);

    // Left shift
    if (operation.equals(Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL)) {
      final Operand.SecretVariable highBits = circuitAccum.addExtract(typeOfWhatToRetain, v1, 0);
      return circuitAccum.addBinop(
          resultType, ZkOperation.BinaryOp.BIT_CONCAT, highBits, shiftedInZeroBits);
      // Right shift
    } else {
      final Operand.SecretVariable lowBits =
          circuitAccum.addExtract(typeOfWhatToRetain, v1, shiftAmount);
      return circuitAccum.addBinop(
          resultType, ZkOperation.BinaryOp.BIT_CONCAT, shiftedInZeroBits, lowBits);
    }
  }

  private static final Map<Operation.BinaryOp, ZkOperation.BinaryOp> SBI_BINOPS =
      Map.ofEntries(
          // Arithmetic
          Map.entry(Operation.BinaryOp.ADD_WRAPPING, ZkOperation.BinaryOp.ARITH_ADD_WRAPPING),
          Map.entry(
              Operation.BinaryOp.MULT_WRAPPING_SIGNED, ZkOperation.BinaryOp.ARITH_MULTIPLY_SIGNED),

          // Comparison
          Map.entry(Operation.BinaryOp.LESS_THAN_SIGNED, ZkOperation.BinaryOp.CMP_LESS_THAN_SIGNED),
          Map.entry(
              Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED,
              ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_SIGNED),
          Map.entry(
              Operation.BinaryOp.LESS_THAN_UNSIGNED, ZkOperation.BinaryOp.CMP_LESS_THAN_UNSIGNED),
          Map.entry(
              Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED,
              ZkOperation.BinaryOp.CMP_LESS_THAN_OR_EQUALS_UNSIGNED),
          Map.entry(Operation.BinaryOp.EQUAL, ZkOperation.BinaryOp.CMP_EQUALS),

          // Bits
          Map.entry(Operation.BinaryOp.BITWISE_AND, ZkOperation.BinaryOp.BITWISE_AND),
          Map.entry(Operation.BinaryOp.BITWISE_OR, ZkOperation.BinaryOp.BITWISE_OR),
          Map.entry(Operation.BinaryOp.BITWISE_XOR, ZkOperation.BinaryOp.BITWISE_XOR),
          Map.entry(Operation.BinaryOp.BIT_CONCAT, ZkOperation.BinaryOp.BIT_CONCAT));

  private static Operand.SecretVariable executeSecretBinary(
      final CircuitAccum circuitAccum,
      final Type.SecretBinaryInteger resultType,
      final Operation.BinaryOp operation,
      final Operand.SecretVariable v1,
      final Operand.SecretVariable v2) {

    // Found binop
    final ZkOperation.BinaryOp binop = SBI_BINOPS.get(operation);
    if (binop != null) {
      return circuitAccum.addBinop(resultType, binop, v1, v2);

      // Unknown
    } else {
      throw new UnsupportedOperationException(
          "Binary Operation not supported: %s %s".formatted(resultType, operation));
    }
  }

  static Operand.SecretVariable executeSecretUnary(
      final CircuitAccum circuitAccum,
      final Type.SecretBinaryInteger resultType,
      final Operation.UnaryOp operation,
      final Operand.SecretVariable v1) {

    // Negation (Subtracting from 0)
    if (Operation.UnaryOp.NEGATE.equals(operation)) {
      return circuitAccum.addUnop(resultType, ZkOperation.UnaryOp.ARITH_NEGATE_SIGNED, v1);

      // Boolean not
    } else if (Operation.UnaryOp.BITWISE_NOT.equals(operation)) {
      return circuitAccum.addUnop(resultType, ZkOperation.UnaryOp.BITWISE_NOT, v1);

      // Unknown
    } else {
      throw new UnsupportedOperationException(
          "Unary Operation not supported: %s %s".formatted(resultType, operation));
    }
  }

  /**
   * Executes the terminator and returns information on how to proceed to the next block. For
   * branch-if, this will evaluate the condition in the terminator, and select a branch based upon
   * the result.
   *
   * @param uncheckedTerminator Terminator to execute.
   * @param environment Environment tracking variables and their gates.
   * @return Branch information, including block id and inputs.
   */
  private static Terminator.BranchInfo evaluateTerminatorForBranchInfo(
      final Terminator uncheckedTerminator, final Environment environment) {
    if (uncheckedTerminator instanceof Terminator.BranchAlways terminator) {
      return terminator.branch();

    } else {
      final Terminator.BranchIf terminator = (Terminator.BranchIf) uncheckedTerminator;
      final Operand uncheckedConstant = environment.get(terminator.conditionVariable());
      if (uncheckedConstant instanceof Operand.PublicConstant operand
          && operand.type().equals(Type.PublicBool)) {
        return PublicValueExecution.evaluateSelect(
            operand, terminator.branchTrue(), terminator.branchFalse());
      } else {
        throw new RuntimeException("Attempting to branch on non-i1 variable!");
      }
    }
  }

  /**
   * Execution environment for lowering a Meta-Circuit. Primarily stores the operand values for
   * variables.
   */
  record Environment(HashMap<VariableId, Operand> variables) {

    /**
     * Creates new empty environment.
     *
     * @return Newly created empty environment.
     */
    public static Environment empty() {
      return new Environment(new HashMap<>());
    }

    /**
     * Adds variable assignment to the environment.
     *
     * @param variableId SecretVariable to add assignment for.
     * @param operandValue Value to assign.
     */
    public void add(final VariableId variableId, final Type type, final Operand operandValue) {
      assertOperandOfCorrectType(variableId, type, operandValue);

      final Operand previousValue = variables.put(variableId, operandValue);

      if (previousValue != null) {
        throw new RuntimeException(
            "Variable $%d is already present in environment".formatted(variableId.variableId()));
      }
    }

    /**
     * Retrieves value from the environment.
     *
     * @param variableId SecretVariable to retrieve assignment for.
     * @return Assigned operand for variable.
     */
    public Operand get(final VariableId variableId) {
      return requireNonNull(variables.get(variableId));
    }
  }

  /**
   * Checks that the given variable declared as the given type is assigned a valid operand.
   *
   * @param variableId Assigned variable.
   * @param assignedType Type of assigned variable.
   * @param operandValue Operand value to assigned.
   */
  static void assertOperandOfCorrectType(
      final VariableId variableId, final Type assignedType, final Operand operandValue) {
    if (!operandValue.type().equals(assignedType)) {
      throw new RuntimeException(
          "Variable $%s expected type %s, but got %s"
              .formatted(variableId.variableId(), assignedType, operandValue.type()));
    }
  }
}
