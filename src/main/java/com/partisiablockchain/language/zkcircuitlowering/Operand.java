package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitPretty;
import com.partisiablockchain.language.zkmetacircuit.Type;
import java.math.BigInteger;

sealed interface Operand {

  //// Interface

  Type type();

  SecretVariable expectVariable();

  PublicConstant expectConstant();

  //// Implementations

  /**
   * Tracks information about the associated secret variable, notably the gate address of the gate
   * producing the variable's value.
   */
  record SecretVariable(ZkAddress gateAddress, Type.SecretBinaryInteger type) implements Operand {

    public SecretVariable {
      requireNonNull(gateAddress);
      requireNonNull(type);
    }

    @Override
    public SecretVariable expectVariable() {
      return this;
    }

    @Override
    public PublicConstant expectConstant() {
      throw new RuntimeException("Expected Operand.PublicConstant, operand was " + this);
    }
  }

  /** Stores value of a publicly executed variable. */
  record PublicConstant(BigInteger value, Type.PublicInteger type) implements Operand {

    public PublicConstant {
      requireNonNull(value);
      requireNonNull(type);
      if (value.signum() < 0) {
        throw new IllegalArgumentException(
            "PublicConstant of type %s cannot contain a negative value: %s"
                .formatted(MetaCircuitPretty.pretty(type), value));
      }
    }

    public static final PublicConstant ZERO =
        new PublicConstant(BigInteger.ZERO, Type.PublicInteger.PublicBool);
    public static final PublicConstant ONE =
        new PublicConstant(BigInteger.ONE, Type.PublicInteger.PublicBool);
    public static final PublicConstant UNIT =
        new PublicConstant(BigInteger.ZERO, new Type.PublicInteger(0));

    @Override
    public SecretVariable expectVariable() {
      throw new RuntimeException("Expected Operand.SecretVariable, operand was " + this);
    }

    @Override
    public PublicConstant expectConstant() {
      return this;
    }
  }
}
