package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkmetacircuit.Operation;
import com.partisiablockchain.language.zkmetacircuit.Type;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

/** Provides methods for the computation of public values in the Meta-Circuit. */
final class PublicValueExecution {

  private PublicValueExecution() {}

  private static final Map<Operation.BinaryOp, BiFunction<BigInteger, BigInteger, BigInteger>>
      BINOPS =
          Map.ofEntries(
              // Bitwise
              Map.entry(Operation.BinaryOp.BITWISE_AND, BigInteger::and),
              Map.entry(Operation.BinaryOp.BITWISE_OR, BigInteger::or),
              Map.entry(Operation.BinaryOp.BITWISE_XOR, BigInteger::xor),
              Map.entry(
                  Operation.BinaryOp.BITSHIFT_RIGHT_LOGICAL,
                  (v1, v2) -> v1.shiftRight(v2.intValue())),
              Map.entry(
                  Operation.BinaryOp.BITSHIFT_LEFT_LOGICAL,
                  (v1, v2) -> v1.shiftLeft(v2.intValue())),

              // Arithmetic binop
              Map.entry(Operation.BinaryOp.ADD_WRAPPING, BigInteger::add),
              Map.entry(Operation.BinaryOp.MULT_WRAPPING_SIGNED, BigInteger::multiply),
              Map.entry(Operation.BinaryOp.MULT_WRAPPING_UNSIGNED, BigInteger::multiply),

              // Relational operations
              Map.entry(
                  Operation.BinaryOp.EQUAL,
                  (v1, v2) -> v1.compareTo(v2) == 0 ? BigInteger.ONE : BigInteger.ZERO),
              Map.entry(
                  Operation.BinaryOp.LESS_THAN_SIGNED,
                  (v1, v2) -> v1.compareTo(v2) < 0 ? BigInteger.ONE : BigInteger.ZERO),
              Map.entry(
                  Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED,
                  (v1, v2) -> v1.compareTo(v2) <= 0 ? BigInteger.ONE : BigInteger.ZERO),
              Map.entry(
                  Operation.BinaryOp.LESS_THAN_UNSIGNED,
                  (v1, v2) -> v1.compareTo(v2) < 0 ? BigInteger.ONE : BigInteger.ZERO),
              Map.entry(
                  Operation.BinaryOp.LESS_THAN_OR_EQUAL_UNSIGNED,
                  (v1, v2) -> v1.compareTo(v2) <= 0 ? BigInteger.ONE : BigInteger.ZERO));

  private static final Set<Operation.BinaryOp> SIGNED_RELATIONAL_BINOPS =
      Set.of(Operation.BinaryOp.LESS_THAN_SIGNED, Operation.BinaryOp.LESS_THAN_OR_EQUAL_SIGNED);

  private static final Map<Operation.UnaryOp, Function<BigInteger, BigInteger>> UNOPS =
      Map.ofEntries(
          Map.entry(Operation.UnaryOp.BITWISE_NOT, BigInteger::not),
          Map.entry(Operation.UnaryOp.NEGATE, BigInteger::negate));

  /**
   * Evaluates a binary operation over public variables.
   *
   * @param resultType Type to fit the result variable to.
   * @param operation Binary operation to perform.
   * @param operand1 First operand
   * @param operand2 Second operand
   * @return Newly produced operand. Never null.
   */
  public static Operand.PublicConstant evaluateBinop(
      final Type.PublicInteger resultType,
      final Operation.BinaryOp operation,
      final Operand.PublicConstant operand1,
      final Operand.PublicConstant operand2) {

    final BigInteger resultValue;

    final BigInteger val1;
    final BigInteger val2;
    if (SIGNED_RELATIONAL_BINOPS.contains(operation)) {
      val1 = toSignedVal(operand1);
      val2 = toSignedVal(operand2);
    } else {
      val1 = operand1.value();
      val2 = operand2.value();
    }

    // General binops
    final BiFunction<BigInteger, BigInteger, BigInteger> operationLambda = BINOPS.get(operation);
    if (operationLambda != null) {
      resultValue = operationLambda.apply(val1, val2);

      // Concat special case
    } else {
      final BigInteger highBits = operand1.value();
      final BigInteger lowBits = operand2.value();
      final int lowBitsSize = operand2.type().width();
      resultValue = highBits.shiftLeft(lowBitsSize).xor(lowBits);
    }

    return fitInType(resultType, resultValue);
  }

  /**
   * Evaluates a unary operation over public variables.
   *
   * @param resultType Type to fit the result variable to.
   * @param operation Unary operation to perform.
   * @param operand1 First and only operand
   * @return Newly produced value. Never null.
   */
  public static Operand.PublicConstant evaluateUnop(
      final Type.PublicInteger resultType,
      final Operation.UnaryOp operation,
      final Operand.PublicConstant operand1) {
    final Function<BigInteger, BigInteger> operationLambda = UNOPS.get(operation);
    return fitInType(resultType, operationLambda.apply(operand1.value()));
  }

  /**
   * Evaluates a select operation over public variables.
   *
   * @param <T> Type of branches and result type.
   * @param valueCnd Value to compare.
   * @param valueThen Result value if valueCnd is not zero.
   * @param valueElse Result value if valueCnd is zero.
   * @return Selected value. Can only ever be one of valueThen or valueElse.
   */
  public static <T> T evaluateSelect(
      final Operand.PublicConstant valueCnd, final T valueThen, final T valueElse) {
    return valueCnd.value().compareTo(BigInteger.ZERO) != 0 ? valueThen : valueElse;
  }

  /**
   * Evaluates an extract operation over public variables.
   *
   * @param value Value to extract from.
   * @param type Type of value to extract.
   * @param offset Offset from low bits.
   * @return Newly extracted value.
   */
  public static Operand.PublicConstant evaluateExtract(
      final Operand.PublicConstant value, final Type.PublicInteger type, final int offset) {
    return fitInType(type, value.value().shiftRight(offset));
  }

  /**
   * Takes an integer and fits it into the given type.
   *
   * <p>Converts an signed {@link BigInteger} to an unsigned {@link BigInteger}.
   *
   * @param type Type to fit into.
   * @param value Value to fit into type.
   * @return Newly created public constant.
   */
  static Operand.PublicConstant fitInType(final Type.PublicInteger type, final BigInteger value) {
    return new Operand.PublicConstant(fitInBits(type.width(), value), type);
  }

  /**
   * Takes an integer and fits it into the given amount of bits.
   *
   * <p>Converts an signed {@link BigInteger} to an unsigned {@link BigInteger}.
   *
   * @param bitWidth Number of bits to fit into.
   * @param number Value to fit.
   * @return Newly fit value.
   */
  static BigInteger fitInBits(final int bitWidth, final BigInteger number) {
    final BigInteger maxPlusOne = BigInteger.ONE.shiftLeft(bitWidth);
    return number.mod(maxPlusOne);
  }

  /**
   * Interprets a public constant as unsigned, and casts it to its signed value.
   *
   * <p>If the highest bit in the value is set (relative to the width of the constant), the value is
   * wrapped around to it's negative value. Otherwise, the value is returned as is.
   *
   * @param constant The public constant containing the value to cast.
   * @return the sign-casted version of the constant value.
   */
  static BigInteger toSignedVal(Operand.PublicConstant constant) {
    final BigInteger val = constant.value();
    final int width = constant.type().width();

    if (val.testBit(width - 1)) {
      return val.subtract(BigInteger.ONE.shiftLeft(width));
    }

    return val;
  }
}
