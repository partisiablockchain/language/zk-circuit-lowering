package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.List;

/** Utility class for performing batched folding. */
final class BatchedFolding {

  private BatchedFolding() {}

  /**
   * Function interface for batched functions to be used with {@link
   * BatchedFolding#foldByBatchedTreePartial}.
   *
   * @param <T> Type of function.
   */
  @FunctionalInterface
  public interface BatchedFoldingFunction<T> {

    /**
     * Computes some function pairwise between the left and the right lists, producing a new list.
     * Element indices are preserved.
     *
     * @param left List of left elements. Not nullable. Must have same length as {@code right}.
     * @param right List of right elements. Not nullable. Must have same length as {@code left}.
     * @return List of computed elements. Not nullable. Must be same length as {@code left}.
     */
    List<T> computePairwiseBatch(List<T> left, List<T> right);
  }

  /**
   * Efficient functional fold function, over batched associate functions.
   *
   * <p>Folds over the given {@code inputs} by repeatedly applying {@code batchFn} to half of the
   * {@code inputs} with the other half, eventually reducing to a single value, which is then
   * output.
   *
   * <p>Due to the unpredictability of the application order, {@code batchFn} should be associative
   * and commutative, as it might otherwise produce unexpected outputs.
   *
   * <p>Additional requirements:
   *
   * <ul>
   *   <li>{@code batchFn} must be a batched function (a function implementing a basic function
   *       {@code fn} more efficiently by doing the same function repeatedly).
   *   <li>{@code batchFn} must take two lists of equal length {@code N}, and returning a new list
   *       of the same length {@code N}. {@code batchFn} will never be called with differently sized
   *       lists.
   *   <li>The basis function {@code fn}, should be associative.
   *   <li>{@code identity} should be an identity element over {@code fn}.
   * </ul>
   *
   * @param <T> Type to fold over.
   * @param numAllowedRemainingElements Number of elements to output. Might output less in certain
   *     cases. {@code 1} and {@code 2} are the most useful.
   * @param inputs Inputs to fold over.
   * @param batchFn Batched function. Unbatched function must be associative.
   * @return Output elements. At between {@code 1} and {@code numAllowedRemainingElements}. Equal to
   *     input if {@code inputs.size() <= numAllowedRemainingElements}.
   * @see <a href="https://en.wikipedia.org/wiki/Fold_%28higher-order_function%29">Fold
   *     (higher-order function)</a>
   * @see <a href="https://en.wikipedia.org/wiki/Associative">Associative property</a>
   */
  public static <T> List<T> foldByBatchedTreePartial(
      final int numAllowedRemainingElements,
      final List<T> inputs,
      final BatchedFoldingFunction<T> batchFn) {
    final ArrayList<T> queue = new ArrayList<T>(inputs);

    while (queue.size() > numAllowedRemainingElements) {
      // Determine batch input
      // First input is skipped if the queue is an odd size; will be included
      // in one of the later rounds.
      final int batchSideSize = queue.size() / 2;
      final List<T> batchLeft =
          queue.subList(queue.size() - 2 * batchSideSize, queue.size() - batchSideSize);
      final List<T> batchRight = queue.subList(queue.size() - batchSideSize, queue.size());

      // Compute batch
      final List<T> batchResult = batchFn.computePairwiseBatch(batchLeft, batchRight);

      if (batchResult.size() != batchSideSize) {
        throw new RuntimeException("batchFn must produce a list with same size as input sizes");
      }

      // Bookkeep queue
      queue.subList(queue.size() - 2 * batchSideSize, queue.size()).clear();
      queue.addAll(batchResult);
    }

    return queue;
  }
}
