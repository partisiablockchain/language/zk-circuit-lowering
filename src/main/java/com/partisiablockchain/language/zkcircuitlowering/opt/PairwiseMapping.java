package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;

/** Utility class for performing pairwise mapping over {@link Iterable}s. */
final class PairwiseMapping {

  private PairwiseMapping() {}

  /**
   * Pairwise map function over given iterables.
   *
   * @param <T> Element type of left iterable.
   * @param <S> Element type of right iterable.
   * @param <R> Element type of output list.
   * @param fn Function to appl
   * @param left Iterable of left operands.
   * @param right Iterable of right operands.
   * @return List of outputs.
   */
  public static <T, S, R> List<R> pairwiseMap(
      final BiFunction<T, S, R> fn, final Iterable<T> left, final Iterable<S> right) {
    final ArrayList<R> result = new ArrayList<>();
    final Iterator<T> iterLeft = left.iterator();
    final Iterator<S> iterRight = right.iterator();
    while (iterLeft.hasNext() && iterRight.hasNext()) {
      result.add(fn.apply(iterLeft.next(), iterRight.next()));
    }
    return result;
  }
}
