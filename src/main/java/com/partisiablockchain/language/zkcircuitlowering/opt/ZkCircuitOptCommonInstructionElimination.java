package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitGate;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * {@link ZkCircuit} Optimization pass for removing instructions that have already been performed.
 *
 * <p>Optimizing pass over {@link ZkCircuit} whereby identical instructions are removed and any
 * references to them are renamed.
 *
 * <p>Will additionally remove certain kinds of identity instructions ({@code AND X X => X}), as
 * such instructions are often a result of this kind of optimization, and may significantly reduce
 * the number of performed multiplications.
 *
 * <p>Example:
 *
 * <pre>
 *   A = AND X Y
 *   B = AND X Y
 *   C = OR A B
 * </pre>
 *
 * <p>can be optimized to:
 *
 * <pre>
 *   A = AND X Y
 *   C = OR A A
 * </pre>
 */
public final class ZkCircuitOptCommonInstructionElimination {

  private ZkCircuitOptCommonInstructionElimination() {}

  /**
   * Optimizes the given {@link ZkCircuit}.
   *
   * @param zkCircuit Circuit to optimize. Is not modified.
   * @return Newly optimized circuit.
   * @see ZkCircuitOptCommonInstructionElimination for details on algorithm.
   */
  public static ZkCircuit optimizeZkCircuit(ZkCircuit zkCircuit) {
    final List<ZkCircuitGate> gates = zkCircuit.gateAddresses().map(zkCircuit::getGate).toList();
    final ZkCircuitOptimized outCircuit = new ZkCircuitOptimized(0);
    final Map<ZkAddress, ZkAddress> renamed = optimizeGates(gates, outCircuit);
    outCircuit.setRoots(zkCircuit.getRoots().stream().map(renamed::get).toList());
    return outCircuit;
  }

  /**
   * Optimizes the given gates by performing common instruction elimination.
   *
   * @param gates Gates to optimize.
   * @param outCircuit Circuit to accumulate optimized instructions into.
   * @return The map of replacements that have been performed over the gates.
   */
  private static Map<ZkAddress, ZkAddress> optimizeGates(
      final Iterable<ZkCircuitGate> gates, final ZkCircuitOptimized outCircuit) {

    // List of all operations that have previously been seen.
    final Map<ZkOperation, ZkAddress> seenOperations = new LinkedHashMap<>();

    // The replacement map
    final Map<ZkAddress, ZkAddress> renamed = new LinkedHashMap<>();

    for (final ZkCircuitGate gate : gates) {

      // Replacing addresses
      final ZkOperation updatedOperation =
          ZkOperationAddressUsage.replaceInOperation(gate.operation(), renamed);

      // Does the operation already exist, or is it an identity?
      ZkAddress alreadySeen = seenOperations.get(updatedOperation);
      if (alreadySeen == null) {
        alreadySeen = identityOperationInputAddress(updatedOperation);
      }

      // Remove gate if either of the above; otherwise emit.
      if (alreadySeen != null) {
        renamed.put(gate.gateAddress(), alreadySeen);
      } else {
        final ZkAddress newGate = outCircuit.add(gate.resultType(), updatedOperation);
        seenOperations.put(updatedOperation, newGate);
        renamed.put(gate.gateAddress(), newGate);
      }
    }

    return renamed;
  }

  /**
   * Finds the address that this operation is performing an identity operation over, or {@code null}
   * if this operation is not a known identity operation format.
   *
   * <p>An identity operation is an operation which results in the same value as it was originally
   * given. These operation can consume significant resources, and increase the difficulty of
   * optimizing.
   *
   * @param uncheckedOperation Operation to check. Not {@code null}able.
   * @return the address of the gate this is an identity operation for, or {@code null} if not an
   *     identity operation.
   */
  public static ZkAddress identityOperationInputAddress(ZkOperation uncheckedOperation) {
    // Select where then and else is identical
    if (uncheckedOperation instanceof ZkOperation.Select operation
        && operation.addrThen().equals(operation.addrElse())) {
      return operation.addrThen();
    }

    // Idempotent binary operations.
    if (uncheckedOperation instanceof ZkOperation.Binary operation
        && IDEMPOTENT_OPERATIONS.contains(operation.operation())
        && operation.addr1().equals(operation.addr2())) {
      return operation.addr1();
    }
    return null;
  }

  /** Set of idempotent operations. Invariant: {@code f(X, X) = X} */
  private static final Set<ZkOperation.BinaryOp> IDEMPOTENT_OPERATIONS =
      Set.of(ZkOperation.BinaryOp.BITWISE_AND, ZkOperation.BinaryOp.BITWISE_OR);
}
