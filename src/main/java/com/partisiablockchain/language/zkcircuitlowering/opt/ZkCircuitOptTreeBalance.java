package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitGate;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link ZkCircuit} Optimization pass for balancing subtrees.
 *
 * <p>Optimizing pass over {@link ZkCircuit} whereby gate subtrees of associative (and commutative)
 * operations are balanced, in order to shorten the critical path, and break dependencies.
 *
 * <p>Synergizes well with {@link ZkCircuitOptClustering}, which can collect operations with broken
 * dependencies together into large parallel gate blocks.
 *
 * <p>Example:
 *
 * <pre>
 *   A = ADD X Y
 *   B = ADD A Z
 *   C = ADD B W
 * </pre>
 *
 * <p>can be rebalanced to:
 *
 * <pre>
 *   A = ADD X Y
 *   B = ADD Z W
 *   C = ADD A B
 * </pre>
 *
 * <p>Preserves the output gate ids.
 */
public final class ZkCircuitOptTreeBalance {

  private ZkCircuitOptTreeBalance() {}

  /**
   * Optimizes the given {@link ZkCircuit}.
   *
   * @param zkCircuit Circuit to optimize. Is not modified.
   * @return Newly optimized circuit.
   * @see ZkCircuitOptTreeBalance for details on algorithm.
   */
  public static ZkCircuit optimizeZkCircuit(ZkCircuit zkCircuit) {

    final List<ZkCircuitGate> gates = zkCircuit.gateAddresses().map(zkCircuit::getGate).toList();

    final var outCircuit = new ZkCircuitOptimized(gates.size());
    outCircuit.setRoots(zkCircuit.getRoots());
    optimizeGates(gates, zkCircuit.getRoots(), outCircuit);

    return outCircuit;
  }

  /**
   * The maximum amount of arguments that is allowed to be collected before emitting the balanced
   * binary op.
   *
   * <p>This constant is motivated by the possibility of bad edge cases which might trigger the
   * collection of an exponential number of arguments, resulting in significant slowdowns. By
   * imposing a maximum amount, it should be possible to avoid too many slowdowns.
   */
  private static final long MAX_NUM_ARGUMENTS_COLLECTED = 100;

  /**
   * Optimizes gates by collecting as many arguments as possible for each operation, and then
   * emitting them as a balanced tree.
   *
   * @param gates Gates to optimize
   * @param roots Roots of the circuit.
   * @param outCircuit Output circuit.
   */
  private static void optimizeGates(
      final Iterable<ZkCircuitGate> gates,
      final List<ZkAddress> roots,
      final ZkCircuitOptimized outCircuit) {

    // Contains mapping from address to binop
    final Map<ZkAddress, ZkOperation.BinaryOp> gateBinops = new LinkedHashMap<>();
    final Map<ZkAddress, ZkType> gateTypes = new LinkedHashMap<>();

    // Stores collected arguments for the given address.
    final Map<ZkAddress, List<ZkAddress>> gateArguments = new LinkedHashMap<>();

    for (final ZkCircuitGate gate : gates) {

      // Determine the arguments needed before emitting this gate.
      final List<ZkAddress> requiredArgumentsForGate =
          ZkOperationAddressUsage.usedAddressesWithDuplicates(gate.operation());

      // If the gate is a binop, there might be a way to optimize it.
      if (gate.operation() instanceof ZkOperation.Binary operation) {

        final ZkOperation.BinaryOp binop = operation.operation();

        final boolean identicalArguments = operation.addr1().equals(operation.addr2());

        if (binop.isAssociativeAndCommutative() && !identicalArguments) {
          gateBinops.put(gate.gateAddress(), binop);

          // Determine what arguments should be collected.
          for (final ZkAddress requiredArg : List.copyOf(requiredArgumentsForGate)) {
            if (gateArguments.get(requiredArg) == null) {
              // Gate argument was not optimizable, or has already been emitted. Ignore it
              continue;
            }

            if (gateBinops.get(requiredArg).equals(binop)) {
              // Argument has the same operation and can be absorbed.
              requiredArgumentsForGate.remove(requiredArg);
              requiredArgumentsForGate.addAll(gateArguments.get(requiredArg));
            } else {
              // Argument cannot be absorbed, and must be emitted.
              emitBinop(outCircuit, requiredArg, gateBinops, gateArguments, gateTypes);
            }
          }

          gateTypes.put(gate.gateAddress(), gate.resultType());
          gateArguments.put(gate.gateAddress(), requiredArgumentsForGate);

          // If the number of argument gates is too much, we emit it
          // immediately, to prevent slowdowns.
          if (requiredArgumentsForGate.size() >= MAX_NUM_ARGUMENTS_COLLECTED) {
            emitBinop(outCircuit, gate.gateAddress(), gateBinops, gateArguments, gateTypes);
          }

          // Skip emitting dependencies and gate.
          continue;
        }
      }

      // Emit gate dependencies
      for (final ZkAddress requiredArg : requiredArgumentsForGate) {
        emitBinop(outCircuit, requiredArg, gateBinops, gateArguments, gateTypes);
      }

      // Finally emit the gate
      outCircuit.add(gate.gateAddress(), gate.resultType(), gate.operation());
    }

    // Emit roots
    for (final ZkAddress rootAddress : roots) {
      emitBinop(outCircuit, rootAddress, gateBinops, gateArguments, gateTypes);
    }
  }

  /**
   * Enforces that the operation should either already have been output, or will be output.
   *
   * @param outCircuit Circuit to accumulate to.
   * @param gateAddress Address of operation to emit.
   * @param gateBinops Map of binops for looking up gate.
   * @param gateArguments To lookup arguments for gate.
   * @param gateTypes To lookup gate types.
   */
  private static void emitBinop(
      final ZkCircuitOptimized outCircuit,
      final ZkAddress gateAddress,
      final Map<ZkAddress, ZkOperation.BinaryOp> gateBinops,
      final Map<ZkAddress, List<ZkAddress>> gateArguments,
      final Map<ZkAddress, ZkType> gateTypes) {
    final var args = gateArguments.get(gateAddress);
    if (args != null) {
      outputBalancedBinop(
          outCircuit, gateAddress, gateTypes.get(gateAddress), gateBinops.get(gateAddress), args);
    }
    gateBinops.remove(gateAddress);
    gateTypes.remove(gateAddress);
    gateArguments.remove(gateAddress);
  }

  /**
   * Produces a balanced output of the binary operation. Operation must be associative and
   * commutative.
   *
   * @param outCircuit Circuit to output to.
   * @param outputGateAddress The specific gate we want to emit.
   * @param resultType The result type of the final gate.
   * @param binop Binop we want to emit a balanced tree for. Should be one listed in {@link
   *     ZkOperationAddressUsage#ASSOCIATIVE_AND_COMMUTATIVE_BINOPS}.
   * @param arguments Arguments of the chained operation. Must be longer than 2 entries.
   */
  private static void outputBalancedBinop(
      final ZkCircuitOptimized outCircuit,
      final ZkAddress outputGateAddress,
      final ZkType resultType,
      final ZkOperation.BinaryOp binop,
      final List<ZkAddress> arguments) {

    // Emit balanced tree reduced to two arguments.
    final List<ZkAddress> balancedTreeArguments =
        BatchedFolding.foldByBatchedTreePartial(
            2,
            arguments,
            (ls1, ls2) ->
                PairwiseMapping.pairwiseMap(
                    (e1, e2) ->
                        outCircuit.add(resultType, ZkOperation.Binary.newNormalized(binop, e1, e2)),
                    ls1,
                    ls2));

    // Emit last binop to outputGateAddress.
    outCircuit.add(
        outputGateAddress,
        resultType,
        ZkOperation.Binary.newNormalized(
            binop, balancedTreeArguments.get(0), balancedTreeArguments.get(1)));
  }
}
