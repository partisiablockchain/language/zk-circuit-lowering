package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuitAppendable;
import com.partisiablockchain.language.zkcircuit.ZkCircuitGate;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Stream;

/** A zero-knowledge circuit capable of performing a zero-knowledge computation. */
public final class ZkCircuitOptimized implements ZkCircuitAppendable {

  /** The gates of the circuit. The index in the list is the address of each gate. */
  private final LinkedHashMap<ZkAddress, ZkCircuitGate> gates;

  /** The addresses of the circuit root gates. */
  private List<ZkAddress> rootGateAddresses;

  /** The next unused address to use. */
  private int nextUnusedAddress;

  /**
   * Creates a new ZK circuit.
   *
   * @param nextUnusedAddress The next unused address to use.
   */
  public ZkCircuitOptimized(int nextUnusedAddress) {
    this.gates = new LinkedHashMap<>();
    this.nextUnusedAddress = nextUnusedAddress;
  }

  @Override
  public ZkCircuitGate getGate(final ZkAddress address) {
    return requireNonNull(gates.get(address));
  }

  /**
   * Determines whether the given gate address is used for a gate in this circuit.
   *
   * @param address Address to check for.
   * @return true if and only if the given address is assigned to a gate.
   */
  boolean hasGate(final ZkAddress address) {
    return gates.get(address) != null;
  }

  @Override
  public List<ZkAddress> getRoots() {
    return rootGateAddresses;
  }

  @Override
  public Stream<ZkAddress> gateAddresses() {
    return gates.keySet().stream();
  }

  @Override
  public void setRoots(List<ZkAddress> rootGateAddresses) {
    this.rootGateAddresses = List.copyOf(rootGateAddresses);
  }

  /**
   * Adds all the given gates to the circuit unmodified.
   *
   * @param gates Iterable to take gates from.
   */
  public void addAll(final Iterable<ZkCircuitGate> gates) {
    for (ZkCircuitGate g : gates) {
      add(g.gateAddress(), g.resultType(), g.operation());
    }
  }

  /**
   * Add a {@link ZkCircuitGate} to the circuit with an explicit address.
   *
   * @param address Address of gate.
   * @param resultType Type of value produced by gate.
   * @param operation Operation of the added gate.
   * @return The address of the newly added gate
   * @exception NullPointerException If the given operation is null.
   */
  public ZkAddress add(final ZkAddress address, ZkType resultType, final ZkOperation operation) {
    if (gates.containsKey(address)) {
      throw new RuntimeException(
          "Cannot overwrite the given gate address: %s".formatted(address.address()));
    }
    gates.put(address, new ZkCircuitGate(address, resultType, operation));
    return address;
  }

  @Override
  public ZkAddress add(final ZkType resultType, final ZkOperation operation) {
    final ZkAddress address = new ZkAddress(nextUnusedAddress++);
    return add(address, resultType, operation);
  }
}
