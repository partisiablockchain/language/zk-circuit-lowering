package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** Utility methods for working with {@link ZkOperation}s. */
public final class ZkOperationAddressUsage {

  private ZkOperationAddressUsage() {}

  /**
   * Replaces addresses in the given operation with new addresses specified in the given map.
   *
   * @param uncheckedOperation Operation to replace in.
   * @param renamed Replacement map to use.
   * @return New operation with replaced addresses.
   */
  public static ZkOperation replaceInOperation(
      final ZkOperation uncheckedOperation, final Map<ZkAddress, ZkAddress> renamed) {
    if (uncheckedOperation instanceof ZkOperation.Unary operation) {
      return new ZkOperation.Unary(operation.operation(), renamed.get(operation.addr1()));
    } else if (uncheckedOperation instanceof ZkOperation.SignExtend operation) {
      return new ZkOperation.SignExtend(operation.resultType(), renamed.get(operation.addr1()));
    } else if (uncheckedOperation instanceof ZkOperation.Binary operation) {
      return ZkOperation.Binary.newNormalized(
          operation.operation(), renamed.get(operation.addr1()), renamed.get(operation.addr2()));
    } else if (uncheckedOperation instanceof ZkOperation.Select operation) {
      return new ZkOperation.Select(
          renamed.get(operation.addrCond()),
          renamed.get(operation.addrThen()),
          renamed.get(operation.addrElse()));
    } else if (uncheckedOperation instanceof ZkOperation.Extract operation) {
      return new ZkOperation.Extract(
          renamed.get(operation.addr1()), operation.bitwidth(), operation.bitOffset());
    }
    return uncheckedOperation;
  }

  /**
   * Determines the addresses used by the given {@link ZkOperation}, possibly including duplicates.
   *
   * @param uncheckedOperation Operation to determine used addresses for.
   * @return List of used addresses with duplicates.
   */
  public static List<ZkAddress> usedAddressesWithDuplicates(ZkOperation uncheckedOperation) {
    final List<ZkAddress> used = new ArrayList<ZkAddress>();
    usedAddresses(uncheckedOperation, used);
    return used;
  }

  /**
   * Determines the addresses used by the given {@link ZkOperation}.
   *
   * @param uncheckedOperation Operation to determine used addresses for.
   * @return Set of used addresses.
   */
  public static Set<ZkAddress> usedAddresses(final ZkOperation uncheckedOperation) {
    final Set<ZkAddress> used = new HashSet<ZkAddress>();
    usedAddresses(uncheckedOperation, used);
    return used;
  }

  /**
   * Determines the addresses used by the given {@link ZkOperation}, and accumulates them in the
   * given collection.
   *
   * @param uncheckedOperation Operation to determine used addresses for.
   * @param accum {@link Collection} to accumulate in.
   */
  private static void usedAddresses(
      final ZkOperation uncheckedOperation, final Collection<ZkAddress> accum) {
    if (uncheckedOperation instanceof ZkOperation.Unary operation) {
      accum.add(operation.addr1());
    } else if (uncheckedOperation instanceof ZkOperation.SignExtend operation) {
      accum.add(operation.addr1());
    } else if (uncheckedOperation instanceof ZkOperation.Binary operation) {
      accum.add(operation.addr1());
      accum.add(operation.addr2());
    } else if (uncheckedOperation instanceof ZkOperation.Select operation) {
      accum.add(operation.addrCond());
      accum.add(operation.addrThen());
      accum.add(operation.addrElse());
    } else if (uncheckedOperation instanceof ZkOperation.Extract operation) {
      accum.add(operation.addr1());
    }
  }
}
