package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.zkcircuitlowering.PublicValueExecution.toSignedVal;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkmetacircuit.Type;
import com.partisiablockchain.language.zkmetacircuit.VariableId;
import java.math.BigInteger;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Coverage testing of specific components that are difficult to hit from normal intput. */
public final class CoverageTest {

  @Test
  public void operandCoverage() {
    assertThat(new Operand.SecretVariable(new ZkAddress(662), Type.Sbit))
        .extracting(Operand.SecretVariable::gateAddress)
        .extracting(ZkAddress::address)
        .isEqualTo(662);
    assertThat(new Operand.PublicConstant(BigInteger.valueOf(99), Type.PublicBool).value())
        .isEqualTo(99);
    assertThat(new Operand.PublicConstant(BigInteger.valueOf(0), Type.PublicBool).type())
        .isEqualTo(Type.PublicBool);
  }

  private static Operand.PublicConstant i32(int value) {
    return new Operand.PublicConstant(BigInteger.valueOf(value), new Type.PublicInteger(32));
  }

  @Test
  public void evaluateSelect() {
    assertThat(PublicValueExecution.evaluateSelect(i32(0), "Hello", "World")).isEqualTo("World");
    assertThat(PublicValueExecution.evaluateSelect(i32(1), "Hello", "World")).isEqualTo("Hello");
    assertThat(PublicValueExecution.evaluateSelect(i32(2), "Hello", "World")).isEqualTo("Hello");
  }

  @Test
  public void publicConstantMustNeverBeNull() {
    assertThatThrownBy(() -> i32(-1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("PublicConstant of type i32 cannot contain a negative value: -1");
  }

  @Test
  public void fitInTypeUnit() {
    for (int i = 0; i < 100; i++) {
      assertThat(PublicValueExecution.fitInType(new Type.PublicInteger(0), BigInteger.valueOf(i)))
          .extracting(Operand.PublicConstant::value)
          .extracting(BigInteger::intValue)
          .isEqualTo(0);
    }
  }

  @Test
  public void fitInTypeBool() {
    for (int i = 0; i < 100; i++) {
      assertThat(PublicValueExecution.fitInType(Type.PublicBool, BigInteger.valueOf(i)))
          .extracting(Operand.PublicConstant::value)
          .extracting(BigInteger::intValue)
          .isEqualTo(i & 1);
    }
  }

  @Test
  public void fitInType2Bit() {
    for (int i = 0; i < 100; i++) {
      assertThat(PublicValueExecution.fitInType(new Type.PublicInteger(2), BigInteger.valueOf(i)))
          .extracting(Operand.PublicConstant::value)
          .extracting(BigInteger::intValue)
          .isEqualTo(i & 3);
    }
  }

  @Test
  public void fitInType32Bit() {
    for (int i = 0; i < 1000; i++) {
      assertThat(PublicValueExecution.fitInType(new Type.PublicInteger(32), BigInteger.valueOf(i)))
          .extracting(Operand.PublicConstant::value)
          .extracting(BigInteger::intValue)
          .isEqualTo(i);
    }
  }

  @Test
  public void fitInTypeBig() {
    for (int i = 0; i < 511; i++) {
      assertThat(PublicValueExecution.fitInType(new Type.PublicInteger(i), BigInteger.valueOf(i)))
          .extracting(Operand.PublicConstant::value)
          .extracting(BigInteger::intValue)
          .isEqualTo(i);
    }
  }

  @Test
  public void fitInTypeExhaustiveTest() {
    for (int v = 0; v < 10000; v++) {
      assertThat(
              PublicValueExecution.fitInType(new Type.PublicInteger(8), BigInteger.valueOf(v))
                  .value())
          .as("Input " + v)
          .isNotNegative();
    }
  }

  @Test
  public void environment() {
    final var env = MetaCircuitLowering.Environment.empty();
    env.add(new VariableId(1), Type.Sbit, new Operand.SecretVariable(new ZkAddress(32), Type.Sbit));
    env.add(
        new VariableId(2),
        Type.PublicBool,
        new Operand.PublicConstant(BigInteger.valueOf(1), Type.PublicBool));

    assertThat(env.variables())
        .containsEntry(new VariableId(1), new Operand.SecretVariable(new ZkAddress(32), Type.Sbit))
        .containsEntry(
            new VariableId(2), new Operand.PublicConstant(BigInteger.valueOf(1), Type.PublicBool));

    assertThatThrownBy(
            () ->
                env.add(
                    new VariableId(2),
                    Type.PublicBool,
                    new Operand.PublicConstant(BigInteger.valueOf(0), Type.PublicBool)))
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("Variable $2 is already present in environment");
  }

  @Test
  public void typeWidth() {
    final List<Type> types =
        List.of(
            Type.Sbit,
            Type.PublicBool,
            Type.Unit,
            new Type.PublicInteger(123),
            new Type.SecretBinaryInteger(123));

    for (final Type t : types) {
      assertThat(MetaCircuitLowering.setWidth(t, t.width())).isEqualTo(t);
      assertThat(MetaCircuitLowering.setWidth(t, 32).width()).isEqualTo(32);
    }
  }

  @Test
  public void unsignedBigInteger() {
    for (int i = 0; i < 127; i++) {
      assertThat(MetaCircuitLowering.unsignedBigInteger(0, i)).isEqualTo(0);
    }
    for (int i = 0; i < 127; i++) {
      assertThat(MetaCircuitLowering.unsignedBigInteger(i, 8)).isEqualTo(i);
    }
    for (int i = 1; i < 127; i++) {
      assertThat(MetaCircuitLowering.unsignedBigInteger(-i, 8)).isEqualTo(256 - i);
    }
    for (int i = 0; i < 127; i++) {
      assertThat(MetaCircuitLowering.unsignedBigInteger(i, 16)).isEqualTo(i);
    }
    for (int i = 1; i < 127; i++) {
      assertThat(MetaCircuitLowering.unsignedBigInteger(-i, 16)).isEqualTo(65536 - i);
    }
  }

  @Test
  public void unsignedPublicConstant() {
    for (int i = 0; i < 127; i++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(
                      BigInteger.valueOf(0), new Type.PublicInteger(i))
                  .value())
          .isEqualTo(0);
    }
    for (int i = 0; i < 127; i++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(
                      BigInteger.valueOf(i), new Type.PublicInteger(8))
                  .value())
          .isEqualTo(i);
    }
    for (int i = 1; i < 127; i++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(
                      BigInteger.valueOf(-i), new Type.PublicInteger(8))
                  .value())
          .isEqualTo(256 - i);
    }
    for (int i = 0; i < 127; i++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(
                      BigInteger.valueOf(i), new Type.PublicInteger(16))
                  .value())
          .isEqualTo(i);
    }
    for (int i = 1; i < 127; i++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(
                      BigInteger.valueOf(-i), new Type.PublicInteger(16))
                  .value())
          .isEqualTo(65536 - i);
    }

    final var huge = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
    assertThat(MetaCircuitLowering.unsignedPublicConstant(huge, new Type.PublicInteger(8)).value())
        .isEqualTo(255);
    assertThat(MetaCircuitLowering.unsignedPublicConstant(huge, new Type.PublicInteger(16)).value())
        .isEqualTo(65535);

    for (int len = 0; len < 50 * 8; len++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(huge, new Type.PublicInteger(len)).value())
          .isNotNegative();
    }
  }

  @Test
  public void unsignedBigIntegerExhaustive() {
    for (int v = -10000; v < 10000; v++) {
      assertThat(MetaCircuitLowering.unsignedBigInteger(v, 8)).as("Input " + v).isNotNegative();
    }
  }

  @Test
  public void unsignedPublicConstantExhaustiveTest() {
    for (int v = -10000; v < 10000; v++) {
      assertThat(
              MetaCircuitLowering.unsignedPublicConstant(
                      BigInteger.valueOf(v), new Type.PublicInteger(8))
                  .value())
          .as("Input " + v)
          .isNotNegative();
    }
  }

  /**
   * If a constant which doesn't utilize its full width is interpreted as signed, its value doesn't
   * change.
   */
  @Test
  public void toSignedNoChangeWhenNarrow() {
    final int maxWidth = 16;
    final int maxNum = (1 << (maxWidth - 1)) - 1; // 2^15 - 1

    for (int i = 0; i < maxNum; i++) {
      BigInteger val = BigInteger.valueOf(i);

      Operand.PublicConstant constant =
          new Operand.PublicConstant(val, new Type.PublicInteger(maxWidth));

      assertThat(toSignedVal(constant)).isEqualTo(val);
    }
  }

  /**
   * If a constant utilizes its full width (all bits), interpreting it as signed converts it to its
   * signed two complement equivalent.
   */
  @Test
  public void toSignedNegativeWhenFull() {
    final int maxWidth = 16;

    List<BigInteger> unsignedVals =
        List.of(
            BigInteger.valueOf(32768), // 2^15
            BigInteger.valueOf(49152), // 2^15 + 2^14
            BigInteger.valueOf(65535) // 2^16 - 1
            );
    List<BigInteger> expectedSignedVals =
        List.of(
            BigInteger.valueOf(-32768), // -(2^15)
            BigInteger.valueOf(-16384), // -(2^14)
            BigInteger.valueOf(-1) // -1
            );

    for (int i = 0; i < unsignedVals.size(); i++) {
      Operand.PublicConstant constant =
          new Operand.PublicConstant(unsignedVals.get(i), new Type.PublicInteger(maxWidth));

      assertThat(toSignedVal(constant)).isEqualTo(expectedSignedVals.get(i));
    }
  }
}
