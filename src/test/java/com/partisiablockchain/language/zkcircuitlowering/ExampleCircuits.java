package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitPrettyParser;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/** Contains various example circuits / programs. */
final class ExampleCircuits {

  /** Contains information on individual test cases: Their filenames and input data. */
  public record TestCase(Path filepath, MetaCircuit mc) {
    @Override
    public String toString() {
      return "TestCase(%s)".formatted(filepath);
    }
  }

  static final List<TestCase> VALID_OPT_BALANCING =
      filesInDirectory("mc_valid/opt_balancing").stream()
          .map(filepath -> new TestCase(filepath, loadCircuitCase(filepath)))
          .toList();

  static final List<TestCase> VALID_OPT_CLUSTERING =
      filesInDirectory("mc_valid/opt_clustering").stream()
          .map(filepath -> new TestCase(filepath, loadCircuitCase(filepath)))
          .toList();

  static final List<TestCase> VALID_OPT_CIE =
      filesInDirectory("mc_valid/opt_cie").stream()
          .map(filepath -> new TestCase(filepath, loadCircuitCase(filepath)))
          .toList();

  /** Valid Meta-Circuits which should be lowerable. */
  static final List<TestCase> VALID_MISC =
      Stream.concat(
              filesInDirectory("mc_valid/zk-compiler").stream(),
              filesInDirectory("mc_valid/original").stream())
          .map(filepath -> new TestCase(filepath, loadCircuitCase(filepath)))
          .toList();

  /** Valid Meta-Circuits which should be lowerable. */
  static final List<TestCase> VALID =
      Stream.of(VALID_OPT_BALANCING, VALID_OPT_CLUSTERING, VALID_OPT_CIE, VALID_MISC)
          .flatMap(List::stream)
          .toList();

  /** Valid Meta-Circuits which are not lowerable. */
  static final List<TestCase> WIP =
      filesInDirectory("mc_wip").stream()
          .map(filepath -> new TestCase(filepath, loadCircuitCase(filepath)))
          .toList();

  /** Invalid Meta-Circuits which should fail during lowering. */
  static final List<TestCase> INVALID =
      filesInDirectory("mc_invalid").stream()
          .map(filepath -> new TestCase(filepath, loadCircuitCase(filepath)))
          .toList();

  private static List<Path> filesInDirectory(final String directoryName) {
    final Path resourceRoot = new File(ExampleCircuits.class.getResource("").getPath()).toPath();
    final File directory = new File(ExampleCircuits.class.getResource(directoryName).getPath());
    final ArrayList<Path> files =
        new ArrayList<>(
            java.util.Arrays.stream(directory.listFiles())
                .map(File::toPath)
                .map(filepath -> resourceRoot.relativize(filepath))
                .toList());
    Collections.sort(files);
    return List.copyOf(files);
  }

  private static String loadProgramText(final Path filepath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> ExampleCircuits.class.getResourceAsStream(filepath.toString()),
            InputStream::readAllBytes,
            "Could not load file: " + filepath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }

  private static MetaCircuit loadCircuitCase(final Path filepath) {
    final String programText = loadProgramText(filepath);
    return ExceptionConverter.call(
        () -> MetaCircuitPrettyParser.parse(programText), filepath.toString());
  }
}
