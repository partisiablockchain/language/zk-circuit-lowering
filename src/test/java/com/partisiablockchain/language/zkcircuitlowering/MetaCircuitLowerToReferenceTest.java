package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkCircuitPretty;
import com.partisiablockchain.language.zkcircuit.ZkCircuitValidator;
import com.partisiablockchain.language.zkcircuitlowering.opt.ZkCircuitOptClustering;
import com.partisiablockchain.language.zkcircuitlowering.opt.ZkCircuitOptCommonInstructionElimination;
import com.partisiablockchain.language.zkcircuitlowering.opt.ZkCircuitOptTreeBalance;
import com.secata.tools.coverage.WithResource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/** Tests that Meta-Circuit to ZkCircuit translation works correctly. */
public final class MetaCircuitLowerToReferenceTest {

  private static boolean shouldRegenerateReferences() {
    final var ref = System.getenv("REGENERATE_REFERENCES");
    return ref != null && !ref.isEmpty();
  }

  /**
   * Tests that the given meta-circuit is executed to the reference {@link ZkCircuit}.
   *
   * @param testCase Test case to test for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuits")
  public void executeToReferenceAllOptimizations(final ExampleCircuits.TestCase testCase) {
    executeToReferenceTest(testCase, "O3");
  }

  /**
   * {@link MetaCircuit} must be lowered to the reference {@link ZkCircuit}, when only tree-balacing
   * optimizations are enabled.
   *
   * @param testCase Test case to test for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuitsBalancing")
  public void executeToReferenceTreeBalanceOnly(final ExampleCircuits.TestCase testCase) {
    executeToReferenceTest(testCase, "balanced");
    executeToReferenceTest(testCase, "noopt");
  }

  /**
   * {@link MetaCircuit} must be lowered to the reference {@link ZkCircuit}, when only instruction
   * clusting optimizations are enabled.
   *
   * @param testCase Test case to test for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuitsClusting")
  public void executeToReferenceClusteringOnly(final ExampleCircuits.TestCase testCase) {
    executeToReferenceTest(testCase, "clustering");
    executeToReferenceTest(testCase, "noopt");
  }

  /**
   * {@link MetaCircuit} must be lowered to the reference {@link ZkCircuit}, when only common
   * instruction elimination optimizations are enabled.
   *
   * @param testCase Test case to test for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuitsCie")
  public void executeToReferenceCommonInstructionElminationOnly(
      final ExampleCircuits.TestCase testCase) {
    executeToReferenceTest(testCase, "cie");
    executeToReferenceTest(testCase, "noopt");
  }

  private static void executeToReferenceTest(
      final ExampleCircuits.TestCase testCase, final String suffix) {
    final Path mcFilepath =
        getResourceSourcePath(ExampleCircuits.class, testCase.filepath().toString());
    final Path zkcFilepath =
        getResourceSourcePath(
            ExampleCircuits.class, "ref/%s.%s.zkc".formatted(testCase.filepath(), suffix));

    final MetaCircuitLowering.Result result =
        MetaCircuitLoweringTest.translateMetaCircuitInTestContext(
            testCase.mc(), mcFilepath.toString());

    ZkCircuit zkCircuitResult = result.zkCircuit();
    ZkCircuitValidator.validateZkCircuit(zkCircuitResult, null);

    if (suffix.equals("O3") || suffix.equals("balanced")) {
      zkCircuitResult = ZkCircuitOptTreeBalance.optimizeZkCircuit(zkCircuitResult);
      ZkCircuitValidator.validateZkCircuit(zkCircuitResult, null);
    }
    if (suffix.equals("O3") || suffix.equals("clustering")) {
      zkCircuitResult = ZkCircuitOptClustering.optimizeZkCircuit(zkCircuitResult);
      ZkCircuitValidator.validateZkCircuit(zkCircuitResult, null);
    }
    if (suffix.equals("O3") || suffix.equals("cie")) {
      zkCircuitResult = ZkCircuitOptCommonInstructionElimination.optimizeZkCircuit(zkCircuitResult);
      ZkCircuitValidator.validateZkCircuit(zkCircuitResult, null);
    }

    final String zkCircuitPretty = ZkCircuitPretty.prettyCircuit(zkCircuitResult);
    if (shouldRegenerateReferences()) {
      writeText(zkcFilepath, zkCircuitPretty);
      System.err.println("Regenerated " + zkcFilepath);
    }

    ZkCircuitValidator.validateZkCircuit(zkCircuitResult, null);

    // Validate compiled zkCircuit is as expected
    final String zkCircuitPrettyExpected = loadText(zkcFilepath);
    assertThat(zkCircuitPretty)
        .describedAs(zkcFilepath.toString())
        .isEqualToNormalizingNewlines(zkCircuitPrettyExpected);
  }

  //// Test providers

  static Stream<Arguments> provideLowerableMetaCircuits() {
    return ExampleCircuits.VALID.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideLowerableMetaCircuitsBalancing() {
    return ExampleCircuits.VALID_OPT_BALANCING.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideLowerableMetaCircuitsClusting() {
    return ExampleCircuits.VALID_OPT_CLUSTERING.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideLowerableMetaCircuitsCie() {
    return ExampleCircuits.VALID_OPT_CIE.stream().map(Arguments::of);
  }

  //// Test loading utility

  /**
   * Determine relative path of of the given resource file in the class structure.
   *
   * @param clazz Class to find resource relative to
   * @param filename Filename of the given file
   * @return Newly created path, never null.
   */
  static Path getResourceSourcePath(final Class<?> clazz, final String filename) {
    final String[] split = clazz.getPackageName().split("\\.");
    final Path referencePathDir = Paths.get("./src/test/resources/", split);
    return referencePathDir.resolve(filename);
  }

  static void writeText(final Path filePath, final String text) {
    WithResource.accept(
        () -> {
          // Create any directories required
          Files.createDirectories(filePath.getParent());

          // Write file itself
          return new java.io.PrintWriter(filePath.toFile(), StandardCharsets.UTF_8);
        },
        outputStream -> outputStream.write(text),
        "Could not write to file " + filePath);
  }

  static String loadText(final Path filePath) {
    final byte[] fileBytes =
        WithResource.apply(
            () -> new FileInputStream(filePath.toFile()),
            InputStream::readAllBytes,
            "Could not load file: " + filePath);

    return new String(fileBytes, StandardCharsets.UTF_8);
  }
}
