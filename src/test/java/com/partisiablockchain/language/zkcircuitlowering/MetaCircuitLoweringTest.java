package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.language.zkcircuit.ZkCircuitValidator;
import com.partisiablockchain.language.zkcircuit.ZkType;
import com.partisiablockchain.language.zkmetacircuit.FunctionId;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuit;
import com.partisiablockchain.language.zkmetacircuit.MetaCircuitValidator;
import com.partisiablockchain.language.zkmetacircuit.Type;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/** Tests that Meta-Circuit to ZkCircuit translation works correctly. */
public final class MetaCircuitLoweringTest {

  /** Id of the initial {@link MetaCircuit.Function} during lowering. */
  private static final FunctionId MAIN_FUNCTION_ID = new FunctionId(0);

  /** Weird function id. */
  private static final FunctionId WEIRD_FUNCTION_ID = new FunctionId(999);

  private static final long MAXIMUM_NUMBER_BRANCHES = 100000;

  /**
   * Tests that meta circuit test cases are actually valid, so we don't accidentally code against
   * invalid bytecode.
   *
   * @param testCase Test case to check for validity.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuits")
  public void areTestCaseMetaCircuitEvenWellFormed(final ExampleCircuits.TestCase testCase) {
    ExceptionConverter.run(
        () -> MetaCircuitValidator.validateMetaCircuit(testCase.mc()),
        testCase.filepath().toString());
  }

  private static BigInteger testEnvVariableValue(int varIdx) {
    if (varIdx == 5) {
      return BigInteger.ONE.shiftLeft(180).negate();
    }
    final BigInteger sign = BigInteger.valueOf(1 - 2 * (varIdx & 1));
    return BigInteger.valueOf(varIdx * 100 + varIdx * varIdx).multiply(sign);
  }

  private static LoweringPublicExecutionEnvironment testEnvironment(int numVariables) {
    final List<BigInteger> vars =
        IntStream.range(1, numVariables + 1)
            .mapToObj(MetaCircuitLoweringTest::testEnvVariableValue)
            .toList();
    return new StaticLoweringPublicExecutionEnvironment(vars);
  }

  /**
   * Performs translation of a given meta circuit with some well-defined test inputs.
   *
   * @param mc Meta-circuit to translate.
   * @param name Name of meta circuit for error messages.
   */
  static MetaCircuitLowering.Result translateMetaCircuitInTestContext(MetaCircuit mc, String name) {
    final List<BigInteger> publicInputsTmp =
        Stream.of(255, 0, -1, 1, 128, 1, 100, 5, 12, 67, 42, 61, 24)
            .map(BigInteger::valueOf)
            .toList();

    final MetaCircuit.Function firstFunction = mc.functions().get(0);
    final int numInputsToInput =
        MetaCircuitLowering.publicInputTypes(firstFunction.blocks().get(0)).size();
    final List<BigInteger> publicInputs = publicInputsTmp.subList(0, numInputsToInput);

    final LoweringPublicExecutionEnvironment testEnvironment = testEnvironment(20);

    final MetaCircuitLowering.Result result =
        ExceptionConverter.call(
            () ->
                MetaCircuitLowering.executeMetaCircuit(
                    mc,
                    firstFunction.functionId(),
                    testEnvironment,
                    publicInputs,
                    MAXIMUM_NUMBER_BRANCHES),
            name);
    assertThat(result).isNotNull();
    return result;
  }

  /**
   * Tests that valid test cases can be executed.
   *
   * @param testCase Test case to run for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuits")
  public void canTranslateValidTest(final ExampleCircuits.TestCase testCase) {
    final List<ZkType> expectedZkCircuitOutputGates = null;
    //
    final List<Type.PublicInteger> expectedPublicOutputValues =
        testCase.mc().functions().get(0).returnTypes().stream()
            .filter(t -> t instanceof Type.PublicInteger)
            .map(t -> (Type.PublicInteger) t)
            .toList();

    final MetaCircuitLowering.Result result =
        translateMetaCircuitInTestContext(testCase.mc(), testCase.filepath().toString());

    assertThat(result.outputValues()).hasSize(expectedPublicOutputValues.size());

    ExceptionConverter.run(
        () ->
            ZkCircuitValidator.validateZkCircuit(result.zkCircuit(), expectedZkCircuitOutputGates),
        testCase.filepath().toString());
  }

  /**
   * Tests that valid test cases throw on too many public inputs.
   *
   * @param testCase Test case to run for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuits")
  public void validTestsThrowIfTooManyInputs(final ExampleCircuits.TestCase testCase) {

    // Create too many inputs
    final MetaCircuit.Function firstFunction = testCase.mc().functions().get(0);
    final int expectedNumberOfInputs =
        MetaCircuitLowering.publicInputTypes(firstFunction.blocks().get(0)).size();

    final List<BigInteger> publicInputs =
        IntStream.range(0, expectedNumberOfInputs + 1).boxed().map(BigInteger::valueOf).toList();

    assertThatCode(
            () ->
                MetaCircuitLowering.executeMetaCircuit(
                    testCase.mc(),
                    firstFunction.functionId(),
                    testEnvironment(0),
                    publicInputs,
                    MAXIMUM_NUMBER_BRANCHES))
        .as(testCase.filepath().toString())
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Public input count mismatch.");
  }

  /**
   * Tests that valid test cases throw when set to weird function ids.
   *
   * @param testCase Test case to run for.
   */
  @ParameterizedTest
  @MethodSource("provideLowerableMetaCircuits")
  public void validTestsThrowIfInitToWeirdFunctionId(final ExampleCircuits.TestCase testCase) {

    assertThatCode(
            () ->
                MetaCircuitLowering.executeMetaCircuit(
                    testCase.mc(), WEIRD_FUNCTION_ID, testEnvironment(0), List.of(), 10))
        .as(testCase.filepath().toString())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No function with id 999");
  }

  /**
   * Tests that valid but unsupported test cases throws unsupported exceptions.
   *
   * @param testCase Test case to run for.
   */
  @ParameterizedTest
  @MethodSource("provideWipLowerableMetaCircuits")
  public void wipTranslateTest(final ExampleCircuits.TestCase testCase) {
    final LoweringPublicExecutionEnvironment env =
        new StaticLoweringPublicExecutionEnvironment(List.of());
    assertThatCode(
            () ->
                MetaCircuitLowering.executeMetaCircuit(
                    testCase.mc(), MAIN_FUNCTION_ID, env, List.of(), MAXIMUM_NUMBER_BRANCHES))
        .as(testCase.filepath().toString())
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessageContaining("not supported");
  }

  /**
   * Tests that invalid test cases throws runtime exceptions, and never unsupported exceptions.
   *
   * @param testCase Test case to run for.
   */
  @ParameterizedTest
  @MethodSource("provideInvalidLowerableMetaCircuits")
  public void invalidTranslateTest(final ExampleCircuits.TestCase testCase) {
    final LoweringPublicExecutionEnvironment env =
        new StaticLoweringPublicExecutionEnvironment(List.of());
    assertThatCode(
            () ->
                MetaCircuitLowering.executeMetaCircuit(
                    testCase.mc(), MAIN_FUNCTION_ID, env, List.of(), MAXIMUM_NUMBER_BRANCHES))
        .as(testCase.filepath().toString())
        .isInstanceOf(RuntimeException.class)
        .isNotInstanceOf(UnsupportedOperationException.class);
  }

  static Stream<Arguments> provideLowerableMetaCircuits() {
    return ExampleCircuits.VALID.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideWipLowerableMetaCircuits() {
    return ExampleCircuits.WIP.stream().map(Arguments::of);
  }

  static Stream<Arguments> provideInvalidLowerableMetaCircuits() {
    return ExampleCircuits.INVALID.stream().map(Arguments::of);
  }

  /** Unknown unary operations must result in an error. */
  @Test
  public void executeSecretUnaryUnknownOperation() {
    assertThatCode(() -> MetaCircuitLowering.executeSecretUnary(null, null, null, null))
        .isInstanceOf(UnsupportedOperationException.class)
        .hasMessage("Unary Operation not supported: null null");
  }
}
