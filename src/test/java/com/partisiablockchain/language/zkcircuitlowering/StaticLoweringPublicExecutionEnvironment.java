package com.partisiablockchain.language.zkcircuitlowering;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import java.util.List;

record StaticLoweringPublicExecutionEnvironment(List<BigInteger> variableMetadata)
    implements LoweringPublicExecutionEnvironment {

  @Override
  public BigInteger getSecretVariableMetadata(final int variableId) {
    if (0 < variableId && variableId <= numberOfSecretVariables()) {
      return variableMetadata.get(variableId - 1);
    }
    return null;
  }

  @Override
  public int nextVariableId(int variableId) {
    variableId += 1;
    if (variableId > numberOfSecretVariables()) {
      return 0;
    }
    return variableId;
  }

  @Override
  public int numberOfSecretVariables() {
    return variableMetadata.size();
  }
}
