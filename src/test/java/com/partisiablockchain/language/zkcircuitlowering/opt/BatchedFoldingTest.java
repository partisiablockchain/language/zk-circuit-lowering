package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.function.BiFunction;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link BatchedFolding}. */
public final class BatchedFoldingTest {

  static <T> BatchedFolding.BatchedFoldingFunction<T> wrapPairwise(final BiFunction<T, T, T> map) {
    return (left, right) -> PairwiseMapping.pairwiseMap(map, left, right);
  }

  @Test
  public void foldByBatchedTreePartial() {
    Assertions.assertThat(
            BatchedFolding.foldByBatchedTreePartial(1, List.of(), wrapPairwise(Integer::max)))
        .isEmpty();
    Assertions.assertThat(
            BatchedFolding.foldByBatchedTreePartial(
                1, List.of(5, 23, 42, 34, 324, 321, 32, 45), wrapPairwise(Integer::max)))
        .containsExactly(324);
  }

  @Test
  public void foldWithBadFunction() {
    Assertions.assertThatCode(
            () ->
                BatchedFolding.foldByBatchedTreePartial(1, List.of(1, 1), (ls1, ls2) -> List.of()))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("batchFn must produce a list with same size as input sizes");
  }

  private static String foldStrings(List<String> strings) {
    return BatchedFolding.foldByBatchedTreePartial(1, strings, wrapPairwise("(%s*%s)"::formatted))
        .get(0);
  }

  @Test
  public void foldStrings() {
    Assertions.assertThat(foldStrings(List.of("a"))).isEqualTo("a");
    Assertions.assertThat(foldStrings(List.of("a", "b"))).isEqualTo("(a*b)");
    Assertions.assertThat(foldStrings(List.of("a", "b", "c"))).isEqualTo("(a*(b*c))");
    Assertions.assertThat(foldStrings(List.of("a", "b", "c", "d"))).isEqualTo("((a*c)*(b*d))");
    Assertions.assertThat(foldStrings(List.of("a", "b", "c", "d", "e", "f", "g")))
        .isEqualTo("((a*(c*f))*((b*e)*(d*g)))");
  }
}
