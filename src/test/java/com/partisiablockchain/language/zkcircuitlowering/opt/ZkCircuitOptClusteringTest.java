package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatCode;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkCircuit;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test for {@link ZkCircuitOptClustering}. */
public final class ZkCircuitOptClusteringTest {

  /**
   * Clustering optimization throws errors when no instructions can be emitted, but there is still
   * instructions that must be emitted.
   */
  @Test
  public void throwErrorWhenNoGatesCanBeEmitted() {
    final ZkCircuit circuit = makeCircuitWithSelfReferentialInstruction();
    assertThatCode(() -> ZkCircuitOptClustering.optimizeZkCircuit(circuit))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("No gates to emit!");
  }

  /** Creates a circuit with a self-referential instruction that depends upon itself. */
  private static ZkCircuit makeCircuitWithSelfReferentialInstruction() {
    final ZkCircuitOptimized circuit = new ZkCircuitOptimized(0);
    circuit.add(
        new ZkAddress(1),
        ZkType.SBOOL,
        new ZkOperation.Select(new ZkAddress(1), new ZkAddress(1), new ZkAddress(1)));
    circuit.setRoots(List.of(new ZkAddress(1)));
    return circuit;
  }
}
