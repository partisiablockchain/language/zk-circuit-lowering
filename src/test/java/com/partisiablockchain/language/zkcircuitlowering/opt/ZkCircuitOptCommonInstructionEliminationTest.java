package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import org.junit.jupiter.api.Test;

/** Test for {@link ZkCircuitOptCommonInstructionElimination}. */
public final class ZkCircuitOptCommonInstructionEliminationTest {

  @Test
  public void identityOperationInputAddressTest() {
    final ZkOperation.Select select =
        new ZkOperation.Select(new ZkAddress(1), new ZkAddress(2), new ZkAddress(2));
    assertThat(ZkCircuitOptCommonInstructionElimination.identityOperationInputAddress(select))
        .isNotNull()
        .isEqualTo(new ZkAddress(2));
  }

  @Test
  public void identityOperationInputAddressTest2() {
    final ZkOperation.Select select =
        new ZkOperation.Select(new ZkAddress(1), new ZkAddress(2), new ZkAddress(3));
    assertThat(ZkCircuitOptCommonInstructionElimination.identityOperationInputAddress(select))
        .isNull();
  }
}
