package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import com.partisiablockchain.language.zkcircuit.ZkType;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Coverage testing of specific components that are difficult to hit from normal intput. */
public final class ZkCircuitOptimizedTest {

  @Test
  public void cannotAddSameGate() {
    final ZkCircuitOptimized zkCircuit = new ZkCircuitOptimized(0);
    zkCircuit.add(ZkType.SBOOL, new ZkOperation.Constant(ZkType.SBOOL, List.of(false)));
    assertThatThrownBy(
            () ->
                zkCircuit.add(
                    new ZkAddress(0),
                    ZkType.SBOOL,
                    new ZkOperation.Constant(ZkType.SBOOL, List.of(false))))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Cannot overwrite the given gate address: 0");
  }
}
