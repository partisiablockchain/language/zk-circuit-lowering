package com.partisiablockchain.language.zkcircuitlowering.opt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.zkcircuit.ZkAddress;
import com.partisiablockchain.language.zkcircuit.ZkOperation;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test for {@link ZkOperationAddressUsage}. */
public final class ZkOperationAddressUsageTest {

  /**
   * {@link ZkOperationAddressUsage#usedAddresses} finds all addresses used by the given {@link
   * ZkOperation}.
   */
  @Test
  public void usedAddressesTest() {
    final ZkOperation.Select select =
        new ZkOperation.Select(new ZkAddress(1), new ZkAddress(2), new ZkAddress(2));
    assertThat(ZkOperationAddressUsage.usedAddresses(select))
        .containsExactly(new ZkAddress(1), new ZkAddress(2));
    assertThat(ZkOperationAddressUsage.usedAddressesWithDuplicates(select))
        .containsExactly(new ZkAddress(1), new ZkAddress(2), new ZkAddress(2));
  }

  /**
   * {@link ZkOperationAddressUsage#replaceInOperation} replaces addresses used as keys in the given
   * map with their values.
   */
  @Test
  public void replaceInOperationTest() {
    final ZkOperation.Select selectInput =
        new ZkOperation.Select(new ZkAddress(1), new ZkAddress(2), new ZkAddress(2));
    final ZkOperation.Select selectOutput =
        new ZkOperation.Select(new ZkAddress(11), new ZkAddress(12), new ZkAddress(12));
    final Map<ZkAddress, ZkAddress> replaceMap =
        Map.of(
            new ZkAddress(1), new ZkAddress(11),
            new ZkAddress(2), new ZkAddress(12));

    assertThat(ZkOperationAddressUsage.replaceInOperation(selectInput, replaceMap))
        .isEqualTo(selectOutput);
  }

  @Test
  public void categorizeGateSelectTest() {
    final ZkOperation.Select selectInput =
        new ZkOperation.Select(new ZkAddress(1), new ZkAddress(2), new ZkAddress(2));
    assertThat(ZkCircuitOptClustering.categorizeOperation(selectInput)).isEqualTo("select");
  }
}
