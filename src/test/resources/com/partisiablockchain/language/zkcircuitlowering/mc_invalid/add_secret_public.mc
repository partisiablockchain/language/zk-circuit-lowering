(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi32 $1 (constant 1))
    (sbi32 $2 (add_wrapping $1 $0))
    (branch-always #return $2))))
