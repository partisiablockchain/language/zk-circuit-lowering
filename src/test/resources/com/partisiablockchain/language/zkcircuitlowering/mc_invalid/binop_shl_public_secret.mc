(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 1))
    (sbi32 $1 (constant 1))
    (sbi32 $2 (bitshift_left_logical $0 $1))
    (branch-always #return $2))))
