(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs (sbi32 $0))
    (i32 $2 (constant 66))
    (branch-always #1 $0 $2))
  (block #1
    (inputs (sbi32 $0) (i32 $1))
    (i32 $2 (constant 0))
    (i32 $3 (bitwise_xor $1 $2))
    (i1 $4 (equal $2 $3))
    (branch-if $3
      (0 #return $0)
      (1 #2 $0 $2)))
  (block #2
    (inputs (sbi32 $0) (i32 $1))
    (i32 $2 (constant 0))
    (i32 $3 (bitwise_not $2))
    (i32 $4 (add_wrapping $1 $3))
    (i1 $5 (equal $1 $4))
    (branch-if $5
      (0 #return $0)
      (1 #2 $0 $4)))))
