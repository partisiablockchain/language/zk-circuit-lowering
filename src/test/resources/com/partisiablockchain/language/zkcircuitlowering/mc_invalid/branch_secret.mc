(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (equal $0 $1))
    (branch-if $2
      (0 #return $0)
      (1 #return $1)))))
