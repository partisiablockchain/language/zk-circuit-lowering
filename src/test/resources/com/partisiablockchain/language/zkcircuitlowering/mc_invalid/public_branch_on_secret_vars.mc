(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i1 $1 (constant 1))
    (sbi32 $2 (select $1 $0 $0))
    (branch-always #return $0))))
