(metacircuit
 (function %0
  (output i1)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1  $0)
      (#100)))
  (block #100
    (inputs
      (i1 $1))
    (branch-always #return $1)))
 (function %1
  (output i1)
  (block #0
    (inputs)
    (i32 $0 (constant 1))
    (sbi32 $1 (cast $0))
    (branch-always #return $1))))
