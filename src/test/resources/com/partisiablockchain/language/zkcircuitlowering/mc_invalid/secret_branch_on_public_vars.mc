(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 1))
    (sbi1 $2 (equal $0 $1))
    (i32 $3 (constant 1))
    (sbi32 $4 (select $2 $3 $3))
    (branch-always #return $4))))
