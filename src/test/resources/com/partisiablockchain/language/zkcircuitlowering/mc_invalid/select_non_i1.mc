(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 31))
    (i32 $2 (select $1 $1 $1))
    (branch-always #return $0))))
