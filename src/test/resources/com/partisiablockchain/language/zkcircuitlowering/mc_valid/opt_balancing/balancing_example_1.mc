(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs (sbi32 $1) (sbi32 $2))
    (sbi32 $3 (bitwise_and $1 $2))
    (sbi32 $4 (bitwise_xor $1 $2))
    (sbi32 $5 (bitwise_xor $4 $1))
    (sbi32 $6 (bitwise_and $1 $4))
    (sbi32 $7 (bitwise_xor $3 $5))
    (branch-always #return $6 $7))))

