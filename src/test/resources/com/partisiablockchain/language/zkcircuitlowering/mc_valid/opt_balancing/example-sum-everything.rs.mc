(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 0))
    (sbi32 $1 (cast $0))
    (i32 $2 (num_variables))
    (i32 $3 (constant 1))
    (i32 $4 (add_wrapping $2 $3))
    (i32 $5 (constant 1))
    (branch-always #1 $5 $4 $1))
  (block #1
    (inputs
      (i32 $6)
      (i32 $7)
      (sbi32 $8))
    (i1 $9 (less_than_signed $6 $7))
    (branch-if $9
      (0 #return $8)
      (1 #2 $6 $7 $8)))
  (block #2
    (inputs
      (i32 $10)
      (i32 $11)
      (sbi32 $12))
    (sbi32 $14 (load_variable $10))
    (sbi32 $15 (add_wrapping $12 $14))
    (i32 $16 (constant 1))
    (i32 $17 (add_wrapping $10 $16))
    (branch-always #1 $17 $11 $15))))
