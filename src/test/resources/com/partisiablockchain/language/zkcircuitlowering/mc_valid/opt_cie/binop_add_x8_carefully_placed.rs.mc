(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (add_wrapping $0 $0))
    (sbi32 $2 (add_wrapping $0 $0))
    (sbi32 $3 (add_wrapping $1 $2))
    (sbi32 $4 (add_wrapping $0 $0))
    (sbi32 $5 (add_wrapping $0 $0))
    (sbi32 $6 (add_wrapping $4 $5))
    (sbi32 $7 (add_wrapping $3 $6))
    (branch-always #return $7))))
