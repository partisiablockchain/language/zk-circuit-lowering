(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi64 $1 (bitwise_or $0 $0))
    (sbi64 $2 (bitwise_or $1 $1))
    (sbi64 $3 (bitwise_or $2 $2))
    (sbi64 $4 (bitwise_or $3 $3))
    (sbi64 $5 (bitwise_or $4 $4))
    (sbi64 $6 (bitwise_or $5 $5))
    (sbi64 $7 (bitwise_or $6 $6))
    (sbi64 $8 (bitwise_or $7 $7))
    (sbi64 $9 (bitwise_or $8 $8))
    (branch-always #return $9)))
)
