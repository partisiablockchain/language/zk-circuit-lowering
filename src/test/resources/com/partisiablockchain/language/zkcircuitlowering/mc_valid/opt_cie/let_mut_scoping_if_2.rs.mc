(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i0 $1 (constant 0))
    (i0 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (sbi32 $4 (constant 4))
    (sbi32 $6 (constant 4))
    (sbi32 $9 (constant 4))
    (sbi32 $11 (constant 4))
    (branch-if $3
      (0 #return $11)
      (1 #return $6)))))
