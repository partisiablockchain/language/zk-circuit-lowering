(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i0 $1 (constant 0))
    (i0 $2 (constant 0))
    (i1 $3 (equal $1 $2))
    (sbi32 $4 (constant 4))
    (sbi32 $8 (constant 4))
    (branch-if $3
      (0 #return $0)
      (1 #return $4)))))
