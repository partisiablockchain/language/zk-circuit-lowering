(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant -2147483643))
    (i32 $1 (constant 5))
    (i1 $2 (less_than_or_equal_unsigned $0 $1))
    (sbi32 $3 (constant 21))
    (sbi32 $4 (constant 31))
    (branch-if $2
      (0 #return $3)
      (1 #return $4)))))
