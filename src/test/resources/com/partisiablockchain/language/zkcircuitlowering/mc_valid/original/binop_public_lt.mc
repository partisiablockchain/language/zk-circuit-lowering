(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 11))
    (i32 $1 (constant 11))
    (i1 $2 (less_than_signed $0 $1))
    (sbi32 $3 (constant 0))
    (sbi32 $4 (constant 1))
    (branch-if $2
      (0 #return $3)
      (1 #return $4)))))
