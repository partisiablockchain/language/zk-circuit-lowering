(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 11))
    (i32 $1 (constant 11))
    (i32 $2 (constant 12))
    (i1 $3 (less_than_unsigned $0 $1))
    (i1 $4 (less_than_unsigned $1 $2))
    (i1 $5 (equal $3 $4))
    (sbi32 $6 (constant 0))
    (sbi32 $7 (constant 1))
    (branch-if $5
      (0 #return $6)
      (1 #return $7)))))
