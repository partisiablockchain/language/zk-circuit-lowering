(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant 55))
    (i32 $1 (constant -66))
    (i1 $2 (less_than_signed $0 $1))
    (i1 $3 (less_than_unsigned $0 $1))
    (i1 $4 (equal $2 $3))
    (sbi32 $5 (constant 21))
    (sbi32 $6 (constant 31))
    (branch-if $4
      (0 #return $5)
      (1 #return $6)))))
