(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs (sbi32 $0) (sbi32 $1))
    (i32 $2 (constant 352))
    (i32 $3 (constant 5))
    (i32 $4 (constant 11))
    (i32 $5 (bitshift_right_logical $2 $3))
    (i1 $6 (equal $4 $5))
    (branch-if $6
      (0 #return $0)
      (1 #return $1))))
)
