(metacircuit
 (function %0
  (output sbi16)
  (block #0
    (inputs
      (sbi16 $0))
    (sbi16 $1 (negate $0))
    (branch-always #return $1))))
