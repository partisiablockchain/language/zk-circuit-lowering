(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi64 $1 (negate $0))
    (branch-always #return $1))))
