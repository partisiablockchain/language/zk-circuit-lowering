(metacircuit
 (function %0
  (output sbi8)
  (block #0
    (inputs
      (sbi8 $0))
    (sbi8 $1 (negate $0))
    (branch-always #return $1))))
