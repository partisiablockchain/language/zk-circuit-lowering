(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi64 $1 (add_wrapping $0 $0))
    (sbi64 $2 (add_wrapping $1 $1))
    (sbi64 $3 (add_wrapping $2 $2))
    (sbi64 $4 (add_wrapping $3 $3))
    (sbi64 $5 (add_wrapping $4 $4))
    (sbi64 $6 (add_wrapping $5 $5))
    (sbi64 $7 (add_wrapping $6 $6))
    (sbi64 $8 (add_wrapping $7 $7))
    (sbi64 $9 (add_wrapping $8 $8))
    (branch-always #return $9)))
)
