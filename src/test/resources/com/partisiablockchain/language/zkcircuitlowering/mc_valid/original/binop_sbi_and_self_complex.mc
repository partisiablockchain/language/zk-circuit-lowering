(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi64 $1 (bitwise_and $0 $0))
    (sbi64 $2 (bitwise_and $0 $1))
    (sbi64 $3 (bitwise_and $1 $2))
    (sbi64 $4 (bitwise_and $2 $3))
    (sbi64 $5 (bitwise_and $3 $4))
    (sbi64 $6 (bitwise_and $4 $5))
    (sbi64 $7 (bitwise_and $5 $6))
    (sbi64 $8 (bitwise_and $6 $7))
    (sbi64 $9 (bitwise_and $7 $8))
    (branch-always #return $9)))
)
