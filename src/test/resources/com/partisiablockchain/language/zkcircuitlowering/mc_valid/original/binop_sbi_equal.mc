(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi1 $1 (equal $0 $0))
    (branch-always #return $1))))
