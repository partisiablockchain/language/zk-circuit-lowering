(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_or_equal_signed $0 $1))
    (branch-always #return $2))))
