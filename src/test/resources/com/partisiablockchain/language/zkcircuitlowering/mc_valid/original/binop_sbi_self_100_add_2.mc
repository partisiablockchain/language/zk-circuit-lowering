(metacircuit
 (function %0
  (output sbi10)
  (block #0
    (i32 $3 (constant 0))
    (sbi10 $7 (constant 0))
    (sbi10 $8 (constant 1))
    (branch-always #1 $3 $7 $8))
  (block #1
    (inputs
      (i32 $4)
      (sbi10 $5)
      (sbi10 $7))
    (i32 $6 (constant 101))
    (i1 $8 (equal $4 $6))
    (i1 $9 (bitwise_not $8))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $4 $10))
    (sbi10 $12 (add_wrapping $5 $7))
    (sbi10 $13 (add_wrapping $12 $7))
    (branch-if $9
      (0 #return $13)
      (1 #1 $11 $12 $13)))))
