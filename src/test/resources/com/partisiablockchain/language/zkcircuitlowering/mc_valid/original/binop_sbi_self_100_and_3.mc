(metacircuit
 (function %0
  (output sbi10)
  (block #0
    (i32 $0 (constant 0))
    (sbi10 $1 (constant 421))
    (sbi10 $2 (constant 127))
    (branch-always #1 $0 $1 $2))
  (block #1
    (inputs
      (i32 $4)
      (sbi10 $50)
      (sbi10 $51))
    (i32 $6 (constant 101))
    (i1 $8 (equal $4 $6))
    (i1 $9 (bitwise_not $8))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $4 $10))
    (sbi10 $12 (bitwise_and $50 $51))
    (branch-if $9
      (0 #return $12)
      (1 #1 $11 $12 $51)))))
