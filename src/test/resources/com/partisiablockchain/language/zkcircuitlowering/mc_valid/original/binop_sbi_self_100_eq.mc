(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (i32 $3 (constant 0))
    (sbi1 $7 (constant 1))
    (sbi1 $8 (bitwise_not $7))
    (branch-always #1 $3 $8))
  (block #1
    (inputs
      (i32 $4)
      (sbi1 $5))
    (i32 $6 (constant 101))
    (i1 $8 (equal $4 $6))
    (i1 $9 (bitwise_not $8))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $4 $10))
    (sbi1 $12 (equal $5 $5))
    (branch-if $9
      (0 #return $12)
      (1 #1 $11 $12)))))
