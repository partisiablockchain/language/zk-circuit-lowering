(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs (sbi32 $0))
    (i32 $1 (constant 100000))
    (branch-always #1 $0 $1))
  (block #1
    (inputs (sbi32 $0) (i32 $1))
    (i32 $2 (constant 0))
    (i32 $3 (constant -1))
    (i32 $4 (add_wrapping $1 $3))
    (i1 $5 (equal $4 $2))
    (branch-if $5
      (0 #1 $0 $4)
      (1 #return $0)))))
