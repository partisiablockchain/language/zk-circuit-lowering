(metacircuit
 (function %0
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 95))
    (sbi1 $2 (equal $0 $1))
    (branch-always #return $2))))
