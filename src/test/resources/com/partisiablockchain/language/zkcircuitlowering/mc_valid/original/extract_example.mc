(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (i64 $0))
    (i32 $1 (extract $0 32 0))
    (i32 $2 (extract $0 32 32))
    (i1 $3 (equal $1 $1))
    (i1 $4 (equal $2 $2))
    (i1 $5 (bitwise_and $4 $3))
    (i64 $12 (bit_concat $2 $1))
    (i32 $17 (constant 0))
    (i32 $18 (cast $17))
    (i64 $21 (bit_concat $18 $18))
    (i64 $121 (select $5 $12 $21))
    (sbi64 $122 (cast $121))
    (branch-always #return $122))))
