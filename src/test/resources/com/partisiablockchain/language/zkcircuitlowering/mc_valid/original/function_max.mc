(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%1 $0 $1)
      (#return $0))))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_signed $0 $1))
    (sbi32 $3 (select $2 $1 $0))
    (branch-always #return $3))))

