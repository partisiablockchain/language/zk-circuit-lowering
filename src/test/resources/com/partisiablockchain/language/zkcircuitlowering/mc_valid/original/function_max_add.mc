(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%1 $0 $1)
      (#1 $0)))
  (block #1
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $2 (add_wrapping $0 $1))
    (branch-always #return $0 $2)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (less_than_signed $0 $1))
    (sbi32 $3 (select $2 $1 $0))
    (branch-always #return $3))))

