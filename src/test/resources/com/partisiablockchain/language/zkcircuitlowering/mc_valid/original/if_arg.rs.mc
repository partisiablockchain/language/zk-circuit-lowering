(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi1 $0))
    (sbi1 $1 (cast $0))
    (sbi32 $3 (constant 2))
    (sbi1 $2 (cast $0))
    (sbi32 $6 (constant 4))
    (sbi32 $106 (select $0 $3 $6))
    (branch-always #return $106))))
