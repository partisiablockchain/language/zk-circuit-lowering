(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs (sbi32 $0))
    (sbi32 $1 (constant 0))
    (branch-always #return $1))))
