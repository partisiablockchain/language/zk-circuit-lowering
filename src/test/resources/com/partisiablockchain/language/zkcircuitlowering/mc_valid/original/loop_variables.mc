(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs)
    (i32 $0 (constant 1))
    (branch-always #2 $0))
  (block #1
    (inputs (i32 $0))
    (i32 $1 (next_variable_id $0))
    (i32 $3 (constant 0))
    (i1 $4 (equal $1 $3))
    (branch-if $4
      (0 #2 $1)
      (1 #return $0)))
  (block #2
    (inputs (i32 $0))
    (sbi32 $2 (load_variable $0))
    (branch-always #1 $0))))
