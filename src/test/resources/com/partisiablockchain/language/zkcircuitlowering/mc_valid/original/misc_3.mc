(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (sbi32 $0 (constant 1))
    (sbi32 $1 (bitwise_not $0))
    (sbi32 $2 (bitwise_and $0 $1))
    (sbi32 $3 (bitwise_xor $1 $0))
    (sbi32 $4 (bitwise_or $2 $3))
    (branch-always #return $4))))
