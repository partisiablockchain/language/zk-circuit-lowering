(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0))
    (i0 $1 (output $0))
    (sbi0 $2 (cast $1))
    (sbi32 $3 (bit_concat $0 $2))
    (i0 $4 (output $3))
    (branch-always #return)))
)
