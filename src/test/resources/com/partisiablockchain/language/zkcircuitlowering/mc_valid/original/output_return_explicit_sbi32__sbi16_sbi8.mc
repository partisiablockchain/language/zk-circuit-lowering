(metacircuit
 (function %0
  (output)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi16 $1)
      (sbi8 $2))
    (i0 $3 (output $0))
    (i0 $4 (output $1))
    (i0 $5 (output $2))
    (branch-always #return)))
)
