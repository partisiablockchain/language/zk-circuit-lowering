(metacircuit
 (function %0
  (output sbi32 sbi16 sbi8)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi16 $1)
      (sbi8 $2))
    (branch-always #return $0 $1 $2)))
)
