(metacircuit
 (function %96
  (output sbi32 sbi32 sbi33 sbi33)
  (block #0
    (inputs
      (i1 $0)
      (i32 $1)
      (i1 $2)
      (i32 $3)
      (i32 $4))
    (sbi32 $5 (load_variable $4))
    (call
      (%98 $0 $1)
      (#100 $2 $3 $5)))
  (block #100
    (inputs
      (i1 $370)
      (i32 $371)
      (sbi32 $373)
      (sbi32 $6))
    (call
      (%98 $370 $371)
      (#101 $373 $6)))
  (block #101
    (inputs
      (sbi32 $379)
      (sbi32 $380)
      (sbi32 $7))
    (sbi1 $9 (constant 0))
    (sbi32 $10 (negate $379))
    (sbi32 $11 (add_wrapping $10 $380))
    (call
      (%99 $379)
      (#102 $379 $380 $7 $9 $10 $11)))
  (block #102
    (inputs
      (sbi32 $386)
      (sbi32 $387)
      (sbi32 $388)
      (sbi1 $390)
      (sbi32 $391)
      (sbi32 $392)
      (sbi1 $12))
    (sbi1 $13 (bitwise_not $12))
    (call
      (%99 $392)
      (#103 $386 $387 $388 $390 $391 $13)))
  (block #103
    (inputs
      (sbi32 $398)
      (sbi32 $399)
      (sbi32 $400)
      (sbi1 $402)
      (sbi32 $403)
      (sbi1 $406)
      (sbi1 $14))
    (sbi1 $15 (bitwise_not $14))
    (sbi1 $16 (bitwise_and $15 $406))
    (sbi1 $39 (constant 1))
    (sbi32 $265 (constant 0))
    (sbi32 $266 (select $16 $403 $265))
    (sbi32 $161 (add_wrapping $266 $399))
    (sbi32 $267 (constant 0))
    (sbi32 $268 (select $16 $398 $267))
    (sbi32 $162 (add_wrapping $268 $400))
    (sbi1 $163 (select $16 $39 $402))
    (sbi33 $60 (bit_concat $163 $398))
    (branch-always #return $161 $162 $60 $60)))
 (function %97
  (output sbi32 sbi33)
  (block #0
    (inputs
      (i1 $0)
      (i32 $1)
      (i32 $2))
    (sbi32 $3 (load_variable $2))
    (call
      (%98 $0 $1)
      (#100 $3)))
  (block #100
    (inputs
      (sbi32 $410)
      (sbi32 $4))
    (sbi1 $6 (constant 0))
    (call
      (%99 $410)
      (#101 $410 $4 $6)))
  (block #101
    (inputs
      (sbi32 $414)
      (sbi32 $415)
      (sbi1 $417)
      (sbi1 $7))
    (sbi32 $21 (add_wrapping $414 $415))
    (sbi1 $23 (constant 1))
    (sbi32 $164 (select $7 $415 $21))
    (sbi1 $165 (select $7 $417 $23))
    (sbi33 $36 (bit_concat $165 $414))
    (branch-always #return $164 $36)))
 (function %98
  (output sbi32)
  (block #0
    (inputs
      (i1 $0)
      (i32 $1))
    (branch-if $0
      (0 #2)
      (1 #1 $1)))
  (block #1
    (inputs
      (i32 $3))
    (sbi32 $6 (load_variable $3))
    (branch-always #return $6))
  (block #2
    (sbi32 $11 (constant 0))
    (branch-always #return $11)))
 (function %99
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi1 $1 (extract $0 1 31))
    (branch-always #return $1))))
