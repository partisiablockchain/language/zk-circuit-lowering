(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i1 $1 (constant 1))
    (i1 $2 (select $1 $1 $1))
    (branch-always #return $0))))
