(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs)
    (sbi32 $0 (constant 0))
    (i1 $1 (constant 1))
    (i32 $2 (constant 111))
    (i32 $3 (constant 333))
    (i32 $4 (select $1 $2 $3))
    (branch-always #return $0))))
