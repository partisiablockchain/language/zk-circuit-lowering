(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $3 (constant 1))
    (sbi1 $4 (equal $0 $3))
    (sbi32 $5 (select $4 $1 $1))
    (branch-always #return $5))))
