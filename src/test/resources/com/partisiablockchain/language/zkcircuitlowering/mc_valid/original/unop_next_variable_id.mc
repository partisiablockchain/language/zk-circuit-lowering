(metacircuit
 (function %0
  (output i32)
  (block #0
    (inputs)
    (i32 $0 (constant 1))
    (i32 $1 (next_variable_id $0))
    (branch-always #return $1))))
