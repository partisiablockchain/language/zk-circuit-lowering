(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs (sbi32 $0))
    (i32 $1 (constant 101))
    (i32 $2 (bitwise_not $1))
    (branch-always #return $0))))
