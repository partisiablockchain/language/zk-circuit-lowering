(metacircuit
 (function %0
  (output sbi32 sbi32)
  (block #0
    (inputs (sbi32 $0) (sbi32 $1))
    (i1 $2 (constant 0))
    (i32 $3 (constant 0))
    (i1 $5 (bitwise_not $2))
    (branch-if $5
      (0 #return $0 $1)
      (1 #return $1 $0)))))
