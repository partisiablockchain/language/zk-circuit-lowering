(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (i32 $0 (constant -4))
    (i32 $1 (constant 2147483646))
    (i32 $2 (mult_wrapping_unsigned $0 $1))
    (i32 $3 (constant 8))
    (i1 $4 (equal $2 $3))
    (sbi32 $5 (constant 0))
    (sbi32 $6 (constant 8))
    (branch-if $4
        (0 #return $5)
        (1 #return $6)))))