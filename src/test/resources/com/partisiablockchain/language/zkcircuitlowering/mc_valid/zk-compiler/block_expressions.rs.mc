(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi32 $2 (constant 2))
    (sbi32 $3 (add_wrapping $1 $2))
    (sbi32 $4 (constant 2))
    (sbi32 $5 (add_wrapping $4 $0))
    (sbi32 $6 (add_wrapping $3 $5))
    (sbi32 $7 (constant 2))
    (sbi32 $8 (add_wrapping $7 $6))
    (branch-always #return $8))))
