(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1  $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return $1)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $7)
      (sbi32 $8))
    (i32 $6 (constant 32))
    (i1 $9 (equal $4 $6))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi32 $16)
      (sbi32 $17))
    (call
      (%2  $17 $13)
      (#102 $14 $16 $17)))
  (block #102
    (inputs
      (i32 $344)
      (sbi32 $346)
      (sbi32 $347)
      (sbi1 $20))
    (sbi32 $34 (constant 1))
    (sbi32 $35 (add_wrapping $34 $346))
    (sbi32 $141 (select $20 $35 $346))
    (branch-always #1 $344 $141 $347)))
 (function %2
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (call
      (%3  $1)
      (#100 $0)))
  (block #100
    (inputs
      (sbi32 $349)
      (sbi32 $2))
    (sbi32 $3 (bitwise_and $2 $349))
    (sbi32 $5 (constant 0))
    (sbi1 $6 (equal $3 $5))
    (sbi1 $7 (bitwise_not $6))
    (branch-always #return $7)))
 (function %3
  (output sbi32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 1))
    (i32 $2 (bitshift_left_logical $1 $0))
    (sbi32 $3 (cast $2))
    (branch-always #return $3))))
