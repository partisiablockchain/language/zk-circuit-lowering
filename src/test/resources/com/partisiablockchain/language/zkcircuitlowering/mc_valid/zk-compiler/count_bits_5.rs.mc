(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1  $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return $1)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $7)
      (sbi32 $8))
    (i32 $6 (constant 32))
    (i1 $9 (equal $4 $6))
    (i1 $10 (bitwise_not $9))
    (i32 $11 (constant 1))
    (i32 $12 (add_wrapping $4 $11))
    (branch-if $10
      (0 #return $7)
      (1 #2 $4 $12 $7 $8)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi32 $16)
      (sbi32 $17))
    (call
      (%2  $17 $13)
      (#102 $14 $16 $17)))
  (block #102
    (inputs
      (i32 $226)
      (sbi32 $228)
      (sbi32 $229)
      (sbi1 $20))
    (sbi31 $21 (constant 0))
    (sbi32 $22 (bit_concat $21 $20))
    (sbi32 $23 (add_wrapping $22 $228))
    (branch-always #1 $226 $23 $229)))
 (function %2
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (call
      (%3  $1)
      (#100 $0)))
  (block #100
    (inputs
      (sbi32 $231)
      (i32 $2))
    (sbi32 $3 (cast $2))
    (sbi32 $4 (bitwise_and $3 $231))
    (sbi32 $6 (constant 0))
    (sbi1 $7 (equal $4 $6))
    (sbi1 $8 (bitwise_not $7))
    (branch-always #return $8)))
 (function %3
  (output i32)
  (block #0
    (inputs
      (i32 $0))
    (i32 $1 (constant 1))
    (i32 $2 (bitshift_left_logical $1 $0))
    (branch-always #return $2))))
