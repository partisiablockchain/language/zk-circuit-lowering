(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (call
      (%1 $0)
      (#100)))
  (block #100
    (inputs
      (sbi32 $1))
    (branch-always #return $1)))
 (function %1
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 0))
    (sbi32 $2 (constant 0))
    (branch-always #1 $1 $2 $0))
  (block #1
    (inputs
      (i32 $4)
      (sbi32 $7)
      (sbi32 $8))
    (i32 $6 (constant 32))
    (i1 $10 (equal $4 $6))
    (i1 $11 (bitwise_not $10))
    (i32 $12 (constant 1))
    (i32 $13 (add_wrapping $4 $12))
    (branch-if $11
      (0 #return $7)
      (1 #2 $4 $13 $7 $8)))
  (block #2
    (inputs
      (i32 $14)
      (i32 $15)
      (sbi32 $17)
      (sbi32 $18))
    (sbi1 $25 (extractdyn $18 1 $14))
    (sbi31 $26 (constant 0))
    (sbi32 $27 (bit_concat $26 $25))
    (sbi32 $28 (add_wrapping $17 $27))
    (branch-always #1 $15 $28 $18))))
