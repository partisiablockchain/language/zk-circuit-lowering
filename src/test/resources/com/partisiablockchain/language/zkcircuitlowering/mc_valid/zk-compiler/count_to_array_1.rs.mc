(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (sbi64 $8 (constant 0))
    (i32 $9 (num_variables))
    (i32 $10 (constant 1))
    (i32 $11 (add_wrapping $9 $10))
    (branch-always #1 $10 $11 $8))
  (block #1
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi64 $15))
    (i1 $16 (equal $13 $14))
    (i1 $17 (bitwise_not $16))
    (i32 $18 (constant 1))
    (i32 $19 (add_wrapping $13 $18))
    (branch-if $17
      (0 #return $15)
      (1 #2 $13 $19 $14 $15)))
  (block #2
    (inputs
      (i32 $20)
      (i32 $21)
      (i32 $22)
      (sbi64 $23))
    (sbi8 $25 (load_variable $20))
    (i8 $26 (constant 0))
    (branch-always #4 $26 $25 $21 $22 $23))
  (block #4
    (inputs
      (i8 $27)
      (sbi8 $30)
      (i32 $33)
      (i32 $34)
      (sbi64 $35))
    (i8 $29 (constant 8))
    (i1 $36 (equal $27 $29))
    (i1 $37 (bitwise_not $36))
    (i8 $38 (constant 1))
    (i8 $39 (add_wrapping $27 $38))
    (branch-if $37
      (0 #1 $33 $34 $35)
      (1 #5 $27 $39 $30 $33 $34 $35)))
  (block #5
    (inputs
      (i8 $40)
      (i8 $41)
      (sbi8 $43)
      (i32 $46)
      (i32 $47)
      (sbi64 $48))
    (i24 $55 (constant 0))
    (i32 $56 (bit_concat $55 $40))
    (sbi8 $57 (cast $40))
    (sbi1 $58 (equal $43 $57))
    (sbi7 $59 (constant 0))
    (sbi8 $60 (bit_concat $59 $58))
    (i32 $61 (constant 3))
    (i32 $62 (bitshift_left_logical $56 $61))
    (sbi8 $63 (extractdyn $48 8 $62))
    (sbi8 $64 (add_wrapping $60 $63))
    (sbi64 $67 (insertdyn $48 $64 $62))
    (branch-always #4 $41 $43 $46 $47 $67))))
