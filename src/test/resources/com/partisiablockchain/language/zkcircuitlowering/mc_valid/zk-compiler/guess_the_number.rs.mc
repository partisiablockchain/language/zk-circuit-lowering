(metacircuit
 (function %96
  (output sbi1)
  (block #0
    (inputs
      (i8 $0))
    (sbi8 $1 (cast $0))
    (sbi8 $3 (constant 0))
    (i32 $4 (constant 0))
    (branch-always #1 $4 $1 $3))
  (block #1
    (inputs
      (i32 $5)
      (sbi8 $6)
      (sbi8 $7))
    (i32 $9 (next_variable_id $5))
    (i32 $10 (constant 0))
    (i1 $11 (equal $9 $10))
    (i1 $12 (bitwise_not $11))
    (branch-if $12
      (0 #3 $6 $7)
      (1 #2 $9 $9 $6 $7)))
  (block #2
    (inputs
      (i32 $13)
      (i32 $14)
      (sbi8 $15)
      (sbi8 $16))
    (i8 $21 (load_metadata $13))
    (i8 $22 (constant 0))
    (i1 $23 (equal $21 $22))
    (branch-if $23
      (0 #1 $14 $15 $16)
      (1 #4 $13 $14 $15)))
  (block #4
    (inputs
      (i32 $24)
      (i32 $26)
      (sbi8 $27))
    (sbi8 $36 (load_variable $24))
    (branch-always #1 $26 $27 $36))
  (block #3
    (inputs
      (sbi8 $18)
      (sbi8 $19))
    (sbi1 $53 (equal $18 $19))
    (branch-always #return $53))))

