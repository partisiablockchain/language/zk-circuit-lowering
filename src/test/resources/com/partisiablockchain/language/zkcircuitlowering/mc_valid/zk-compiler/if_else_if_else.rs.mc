(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (less_than_signed $0 $1))
    (sbi32 $3 (cast $0))
    (sbi32 $5 (constant 1))
    (sbi32 $4 (cast $0))
    (sbi32 $8 (constant 5))
    (sbi1 $9 (less_than_signed $4 $8))
    (sbi32 $10 (cast $4))
    (sbi32 $12 (constant 2))
    (sbi32 $11 (cast $4))
    (sbi32 $15 (constant 3))
    (sbi32 $115 (select $9 $12 $15))
    (sbi32 $116 (select $2 $5 $115))
    (branch-always #return $116))))
