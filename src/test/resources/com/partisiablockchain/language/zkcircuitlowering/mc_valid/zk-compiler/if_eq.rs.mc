(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi1 $2 (equal $0 $1))
    (sbi32 $3 (cast $0))
    (sbi32 $4 (cast $1))
    (sbi32 $7 (constant 1))
    (sbi32 $5 (cast $0))
    (sbi32 $6 (cast $1))
    (sbi32 $11 (constant 2))
    (sbi32 $111 (select $2 $7 $11))
    (branch-always #return $111))))
