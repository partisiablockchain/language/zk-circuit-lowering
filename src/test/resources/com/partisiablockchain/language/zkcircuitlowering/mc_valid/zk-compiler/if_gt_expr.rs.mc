(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (less_than_or_equal_signed $1 $0))
    (sbi1 $3 (bitwise_not $2))
    (sbi32 $4 (cast $0))
    (sbi32 $6 (constant 1337))
    (sbi32 $5 (cast $0))
    (sbi32 $9 (constant 0))
    (sbi32 $109 (select $3 $6 $9))
    (branch-always #return $109))))
