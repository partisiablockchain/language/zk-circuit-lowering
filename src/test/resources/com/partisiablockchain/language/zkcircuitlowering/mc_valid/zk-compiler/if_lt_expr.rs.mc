(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (less_than_signed $1 $0))
    (sbi32 $3 (cast $0))
    (sbi32 $5 (constant 1337))
    (sbi32 $4 (cast $0))
    (sbi32 $8 (constant 0))
    (sbi32 $108 (select $2 $5 $8))
    (branch-always #return $108))))
