(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 0))
    (sbi1 $2 (equal $0 $1))
    (branch-always #3 $0))
  (block #3
    (inputs
      (sbi32 $5))
    (sbi32 $7 (constant 1))
    (sbi1 $8 (equal $5 $7))
    (branch-always #6 $5))
  (block #6
    (inputs
      (sbi32 $11))
    (sbi32 $13 (constant 2))
    (sbi1 $14 (equal $11 $13))
    (branch-always #9 $11))
  (block #9
    (inputs
      (sbi32 $17))
    (sbi32 $19 (constant 3))
    (sbi1 $20 (equal $17 $19))
    (branch-always #12 $17))
  (block #12
    (inputs
      (sbi32 $23))
    (sbi32 $25 (constant 4))
    (sbi1 $26 (equal $23 $25))
    (branch-always #15 $23))
  (block #15
    (inputs
      (sbi32 $29))
    (sbi32 $31 (constant 5))
    (sbi1 $32 (equal $29 $31))
    (branch-always #18 $29))
  (block #18
    (inputs
      (sbi32 $35))
    (sbi32 $37 (constant 6))
    (sbi1 $38 (equal $35 $37))
    (branch-always #21 $35))
  (block #21
    (inputs
      (sbi32 $41))
    (sbi32 $43 (constant 7))
    (sbi1 $44 (equal $41 $43))
    (branch-always #24 $41))
  (block #24
    (inputs
      (sbi32 $47))
    (sbi32 $49 (constant 8))
    (sbi1 $50 (equal $47 $49))
    (branch-always #27 $47))
  (block #27
    (inputs
      (sbi32 $53))
    (sbi32 $55 (constant 9))
    (sbi1 $56 (equal $53 $55))
    (branch-always #30 $53))
  (block #30
    (inputs
      (sbi32 $59))
    (sbi32 $61 (constant 10))
    (sbi1 $62 (equal $59 $61))
    (branch-always #33 $59))
  (block #33
    (inputs
      (sbi32 $65))
    (sbi32 $67 (constant 11))
    (sbi1 $68 (equal $65 $67))
    (branch-always #36 $65))
  (block #36
    (inputs
      (sbi32 $71))
    (sbi32 $73 (constant 12))
    (sbi1 $74 (equal $71 $73))
    (branch-always #return $71))))
