(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (equal $0 $1))
    (sbi32 $3 (constant 0))
    (sbi32 $6 (constant 0))
    (sbi32 $4 (cast $3))
    (sbi32 $5 (cast $0))
    (sbi32 $9 (constant 1337))
    (sbi32 $111 (select $2 $9 $6))
    (branch-always #return $111))))
