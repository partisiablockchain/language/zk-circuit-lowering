(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (equal $0 $1))
    (sbi1 $3 (bitwise_not $2))
    (sbi32 $4 (constant 99))
    (sbi32 $7 (constant 99))
    (sbi32 $5 (cast $4))
    (sbi32 $6 (cast $0))
    (sbi32 $10 (constant 0))
    (sbi32 $123 (select $3 $10 $7))
    (sbi32 $124 (select $3 $6 $0))
    (branch-always #3 $123 $124))
  (block #3
    (inputs
      (sbi32 $11)
      (sbi32 $12))
    (sbi32 $15 (constant 3))
    (sbi1 $16 (equal $12 $15))
    (sbi32 $17 (cast $11))
    (sbi32 $18 (cast $12))
    (sbi32 $21 (constant 1337))
    (sbi32 $125 (select $16 $21 $11))
    (branch-always #return $125))))
