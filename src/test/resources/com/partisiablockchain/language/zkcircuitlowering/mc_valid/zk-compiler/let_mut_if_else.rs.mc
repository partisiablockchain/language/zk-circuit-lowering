(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (equal $0 $1))
    (sbi1 $3 (bitwise_not $2))
    (sbi32 $4 (constant 99))
    (sbi32 $7 (constant 99))
    (sbi32 $5 (cast $4))
    (sbi32 $6 (cast $0))
    (sbi32 $10 (constant 0))
    (sbi32 $8 (cast $7))
    (sbi32 $9 (cast $0))
    (sbi32 $13 (constant 1337))
    (sbi32 $113 (select $3 $10 $13))
    (branch-always #return $113))))
