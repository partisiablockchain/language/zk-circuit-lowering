(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi1 $1 (equal $0 $0))
    (sbi32 $2 (cast $0))
    (sbi32 $3 (cast $0))
    (sbi32 $6 (constant 6))
    (sbi32 $4 (cast $0))
    (sbi32 $5 (cast $0))
    (sbi32 $9 (constant 9))
    (sbi32 $109 (select $1 $6 $9))
    (branch-always #return $109))))
