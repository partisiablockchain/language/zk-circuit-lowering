(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi1 $2 (equal $0 $1))
    (sbi32 $3 (constant 2))
    (sbi32 $5 (constant 1))
    (sbi32 $8 (constant 2))
    (sbi32 $10 (constant 1))
    (sbi32 $4 (cast $3))
    (sbi32 $6 (cast $5))
    (sbi32 $7 (cast $0))
    (sbi32 $13 (constant 3))
    (sbi32 $116 (select $2 $13 $8))
    (branch-always #return $116))))
