(metacircuit
 (function %0
  (output sbi128)
  (block #0
    (sbi32 $0 (constant -1))
    (sbi64 $108 (bit_concat $0 $0))
    (sbi128 $118 (bit_concat $108 $108))
    (branch-always #return $118))))
