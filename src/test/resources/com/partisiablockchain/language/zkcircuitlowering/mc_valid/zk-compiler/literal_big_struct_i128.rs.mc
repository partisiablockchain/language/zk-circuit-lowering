(metacircuit
 (function %0
  (output sbi128)
  (block #0
    (i128 $0 (constant -1))
    (sbi128 $1 (cast $0))
    (branch-always #return $1))))
