(metacircuit
 (function %0
  (output sbi128)
  (block #0
    (i32 $0 (constant -1))
    (i64 $108 (bit_concat $0 $0))
    (i128 $118 (bit_concat $108 $108))
    (sbi128 $122 (cast $118))
    (branch-always #return $122))))
