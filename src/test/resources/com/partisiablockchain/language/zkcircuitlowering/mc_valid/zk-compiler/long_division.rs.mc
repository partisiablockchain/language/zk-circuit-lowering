(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (call
      (%1 $0 $1)
      (#100)))
  (block #100
    (inputs
      (sbi64 $2))
    (sbi32 $3 (extract $2 32 0))
    (branch-always #return $3)))
 (function %1
  (output sbi64)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (i32 $2 (constant 0))
    (sbi32 $3 (constant 0))
    (sbi1 $4 (equal $1 $3))
    (branch-always #1 $2 $4 $3 $3 $0 $1))
  (block #1
    (inputs
      (i32 $10)
      (sbi1 $15)
      (sbi32 $16)
      (sbi32 $17)
      (sbi32 $18)
      (sbi32 $19))
    (i32 $12 (constant 32))
    (i1 $20 (equal $10 $12))
    (i1 $21 (bitwise_not $20))
    (i32 $22 (constant 1))
    (i32 $23 (add_wrapping $10 $22))
    (branch-if $21
      (0 #3 $15 $16 $17 $18)
      (1 #2 $10 $23 $15 $16 $17 $18 $19)))
  (block #2
    (inputs
      (i32 $24)
      (i32 $25)
      (sbi1 $28)
      (sbi32 $29)
      (sbi32 $30)
      (sbi32 $31)
      (sbi32 $32))
    (i32 $39 (constant 1))
    (i32 $41 (constant 31))
    (i32 $42 (negate $24))
    (i32 $43 (add_wrapping $41 $42))
    (sbi32 $45 (bitshift_left_logical $30 $39))
    (call
      (%3 $31 $43)
      (#103 $25 $28 $29 $31 $32 $41 $43 $45)))
  (block #103
    (inputs
      (i32 $455)
      (sbi1 $458)
      (sbi32 $459)
      (sbi32 $461)
      (sbi32 $462)
      (i32 $466)
      (i32 $468)
      (sbi32 $469)
      (sbi1 $46))
    (i32 $47 (constant 0))
    (call
      (%2 $469 $47 $46)
      (#104 $455 $458 $459 $461 $462 $466 $468)))
  (block #104
    (inputs
      (i32 $471)
      (sbi1 $474)
      (sbi32 $475)
      (sbi32 $477)
      (sbi32 $478)
      (i32 $482)
      (i32 $484)
      (sbi32 $48))
    (sbi32 $49 (negate $478))
    (sbi32 $50 (add_wrapping $48 $49))
    (call
      (%3 $50 $482)
      (#105 $471 $474 $475 $477 $478 $484 $48 $50)))
  (block #105
    (inputs
      (i32 $489)
      (sbi1 $492)
      (sbi32 $493)
      (sbi32 $495)
      (sbi32 $496)
      (i32 $502)
      (sbi32 $506)
      (sbi32 $508)
      (sbi1 $54))
    (sbi1 $55 (bitwise_not $54))
    (sbi1 $82 (constant 1))
    (call
      (%2 $493 $502 $82)
      (#106 $489 $492 $493 $495 $496 $506 $508 $55)))
  (block #106
    (inputs
      (i32 $510)
      (sbi1 $513)
      (sbi32 $514)
      (sbi32 $516)
      (sbi32 $517)
      (sbi32 $527)
      (sbi32 $529)
      (sbi1 $531)
      (sbi32 $83))
    (sbi32 $246 (select $531 $83 $514))
    (sbi32 $247 (select $531 $529 $527))
    (branch-always #1 $510 $513 $246 $247 $516 $517))
  (block #3
    (inputs
      (sbi1 $34)
      (sbi32 $35)
      (sbi32 $36)
      (sbi32 $37))
    (sbi32 $129 (constant 0))
    (sbi32 $248 (select $34 $129 $35))
    (sbi32 $249 (select $34 $37 $36))
    (sbi64 $146 (bit_concat $249 $248))
    (branch-always #return $146)))
 (function %2
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1)
      (sbi1 $2))
    (sbi32 $5 (insertdyn $0 $2 $1))
    (branch-always #return $5)))
 (function %3
  (output sbi1)
  (block #0
    (inputs
      (sbi32 $0)
      (i32 $1))
    (sbi1 $4 (extractdyn $0 1 $1))
    (branch-always #return $4))))
