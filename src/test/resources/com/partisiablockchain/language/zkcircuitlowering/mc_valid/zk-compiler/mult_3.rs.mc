(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi32 $1 (constant 3))
    (sbi32 $2 (mult_wrapping_signed $0 $1))
    (branch-always #return $2))))
