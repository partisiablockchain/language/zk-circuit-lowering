(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0))
    (sbi32 $1 (extract $0 32 0))
    (sbi32 $2 (extract $0 32 32))
    (sbi1 $3 (equal $1 $1))
    (sbi1 $4 (equal $2 $2))
    (sbi1 $5 (bitwise_and $4 $3))
    (sbi64 $12 (bit_concat $2 $1))
    (i32 $17 (constant 0))
    (sbi32 $18 (cast $17))
    (sbi64 $21 (bit_concat $18 $18))
    (sbi64 $121 (select $5 $12 $21))
    (branch-always #return $121))))
