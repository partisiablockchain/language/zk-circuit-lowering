(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1)
      (sbi32 $2))
    (sbi32 $3 (constant 0))
    (sbi1 $4 (equal $0 $3))
    (sbi1 $5 (bitwise_not $4))
    (sbi32 $6 (select $5 $1 $2))
    (branch-always #return $6))))
