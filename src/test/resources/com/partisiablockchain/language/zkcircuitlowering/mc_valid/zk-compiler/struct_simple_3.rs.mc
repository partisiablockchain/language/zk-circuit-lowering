(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (i32 $1 (constant 3))
    (sbi32 $2 (cast $1))
    (sbi32 $3 (add_wrapping $0 $2))
    (sbi32 $4 (add_wrapping $3 $0))
    (branch-always #return $4))))
