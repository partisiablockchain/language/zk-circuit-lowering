(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0))
    (sbi1 $1 (equal $0 $0))
    (sbi32 $3 (cast $0))
    (sbi32 $6 (constant 0))
    (sbi32 $106 (select $1 $0 $6))
    (branch-always #return $106))))
