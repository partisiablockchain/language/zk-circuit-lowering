(metacircuit
 (function %0
  (output i0)
  (block #0
    (inputs
      (sbi32 $0))
    (i0 $1 (constant 0))
    (branch-always #return $1))))
