(metacircuit
 (function %0
  (output sbi64)
  (block #0
    (inputs
      (sbi64 $0)
      (sbi64 $1))
    (sbi64 $2 (add_wrapping $0 $1))
    (sbi64 $3 (mult_wrapping_signed $2 $1))
    (sbi64 $4 (mult_wrapping_unsigned $3 $1))
    (sbi64 $5 (negate $4))
    (branch-always #return $5))))
