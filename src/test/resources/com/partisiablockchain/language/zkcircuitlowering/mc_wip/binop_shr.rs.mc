(metacircuit
 (function %0
  (output sbi32)
  (block #0
    (inputs
      (sbi32 $0)
      (sbi32 $1))
    (sbi32 $2 (bitshift_right_logical $0 $1))
    (branch-always #return $2))))
